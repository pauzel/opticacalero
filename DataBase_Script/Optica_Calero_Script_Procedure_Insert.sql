/*
	DATABASE OPTICA_CALERO
	SCHEDULED AND PREPARED BY:

	1. ALEJANDRO ANTONIO TELLEZ BARREDA
	2. MELVIN ANTONIO FLORES
	3. PAULINO JOSE ZELAYA TORU�O

	DATABASE PROJECT I
	NATIONAL UNIVERSITY OF ENGINEERING

	..:: ALL THE PROGRAMMING ARE ONLY AND EXCLUSIVELY
	PREPARED BY THE ABOVE MENTIONED PEOPLE ::..

*/

/*
				>>>>>>>>>>	INSERT STORED PROCEDURES <<<<<<<<<<<<<
*/

--=======================================================================================--
--============================USE OPTICA CALERO=======================================--
USE Optica_Calero;
GO
--=======================================================================================--
--==================PROCEDURE TO INSERT NEW USER TYPE ADMIN OR EMPLOYEE==================--
DROP PROCEDURE IF EXISTS UserInsert
GO

CREATE PROCEDURE UserInsert
	@ID_Ced VARCHAR(20),
	@User VARCHAR(20),
	@Password VARCHAR(100),
	@Name VARCHAR(20),
	@LastName VARCHAR(20),
	@Phone VARCHAR(20),
	@Mail VARCHAR(50),
	@Departament VARCHAR(20),
	@Municipality VARCHAR(20),
	@Role VARCHAR(20),
	@Direction VARCHAR(50),
	@RETURN TINYINT OUTPUT,
	@RETURN_MESSAGE VARCHAR(500) OUTPUT
AS
	IF NOT EXISTS(SELECT * FROM dbo.Users u 
				  INNER JOIN dbo.Person p ON  u.ID_Person = p.ID_Person 
				  WHERE p.[Ced] = @ID_Ced OR u.[User] = @User OR p.[Mail] = @Mail OR p.[Phone] = @Phone)
		BEGIN
		------------------------------------------------------
			DECLARE @ID_Person TINYINT
			DECLARE @Ced VARCHAR(50)
			DECLARE @RecoveryCode INT
			SET @RecoveryCode = (SELECT ROUND(RAND()*100000,0))
		-------------------------------------------------
			INSERT INTO dbo.Users([User],[Password],[Department],[Municipality],[Role],[RecoveryCode],[ID_Person])
			VALUES(@User,HASHBYTES('SHA2_256',''+@Password),@Departament,@Municipality,@Role,@RecoveryCode,@ID_Person)
		-------------------------------------------------
			INSERT dbo.Person ([Name],[LastName],[Phone],[Mail],[Direction],[Ced])		
			VALUES(@Name,@LastName,@Phone,@Mail,@Direction,@ID_Ced)
		-------------------------------------------------
			SET @ID_Person = (SELECT ID_Person FROM dbo.Person WHERE [Ced] = @ID_Ced)
			SET @Ced = (SELECT Ced FROM dbo.Person WHERE [Ced] = @ID_Ced)
		-------------------------------------------------
			UPDATE dbo.Users
				SET ID_Person = @ID_Person					
				WHERE [RecoveryCode] = @RecoveryCode
		-------------------------------------------------
			SET @RETURN = 0
			SET @RETURN_MESSAGE = 'Entered correctly'
		END
		ELSE IF EXISTS(SELECT * FROM dbo.Users u 
				  INNER JOIN dbo.Person p ON  u.ID_Person = p.ID_Person 
				  WHERE p.[Ced] = @ID_Ced OR u.[User] = @User OR p.[Mail] = @Mail OR p.[Phone] = @Phone)
		BEGIN
				SET @RETURN = 5
				SET @RETURN_MESSAGE = 'Multiple Fields in use in the database'
			END
			ELSE IF EXISTS(SELECT * FROM dbo.Person WHERE Ced = @ID_Ced)
				BEGIN
					SET @RETURN = 1
					SET @RETURN_MESSAGE = 'ID_Ced already exists in the database'
				END
				ELSE IF EXISTS(SELECT * FROM dbo.Users WHERE [User] = @User)
					BEGIN
						SET @RETURN = 2
						SET @RETURN_MESSAGE = 'User already exists in the database'
					END
					ELSE IF EXISTS(SELECT * FROM dbo.Person WHERE [Mail] = @Mail)
						BEGIN
							SET @RETURN = 3
							SET @RETURN_MESSAGE = 'Mail already exists in the database'
						END
						ELSE IF EXISTS(SELECT * FROM dbo.Person WHERE [Phone] = @Phone)
							BEGIN
								SET @RETURN = 4
								SET @RETURN_MESSAGE = 'Phone already exists in the database'
							END				
GO

DECLARE @N INT,@nm VARCHAR(599)
EXEC UserInsert '0012906980015L','admin1','1234','Jose','Toru�o','50587056445','apaulinojozz@gmail.com','Managua','Managua','Admin','Las Torres',@n output,@nm output
SELECT @N,@nm

SELECT * FROM dbo.Person
SELECT * FROM dbo.Users
--=======================================================================================--
--===================PROCEDURE TO INSERT A NEW CLIENT USER===============================--
DROP PROCEDURE IF EXISTS ClientInsert
GO

CREATE PROCEDURE ClientInsert
	@Ced VARCHAR(30),
	@Name VARCHAR(20),
	@LastName VARCHAR(20),
	@Age TINYINT,
	@Direccion VARCHAR(100),
	@Mail VARCHAR(50),
	@Phone VARCHAR(20),
	@RETURN TINYINT OUTPUT,
	@RETURN_MESSAGE VARCHAR(500) OUTPUT
AS 
	IF NOT EXISTS (SELECT * FROM dbo.Person WHERE Ced=@Ced OR Mail=@Mail OR Phone=@Phone)
		BEGIN 
				DECLARE @ID_Person TINYINT
			-------------------------------------------------------------------
			INSERT INTO [dbo].[Client]([Age],[Ced])
			VALUES(@Age,@Ced)
		--------------------------------------------------------------------
			INSERT dbo.Person ([Name],[LastName],[Phone],[Mail],[Direction],[Ced])		
			VALUES(@Name,@LastName,@Phone,@Mail,@Direccion,@Ced)
		-------------------------------------------------
			SET @ID_Person = (SELECT ID_Person FROM dbo.Person WHERE [Name] = @Name)
		-----------------------------------------------------------------
			UPDATE dbo.Client
				SET ID_Person = @ID_Person
				WHERE Ced = @Ced
		-------------------------------------------------
				SET @RETURN=0
				SET @RETURN_MESSAGE='Entered correctly'
		END
		ELSE IF EXISTS (SELECT * FROM dbo.Person WHERE Ced=@Ced AND Mail=@Mail AND Phone=@Phone)
			BEGIN 
				SET @RETURN=1
				SET @RETURN_MESSAGE='Multiple Fields in use in the database'
			END
			ELSE IF EXISTS (SELECT * FROM dbo.Person WHERE Ced=@Ced )
				BEGIN 
					SET @RETURN=2
					SET @RETURN_MESSAGE='Your ID already exists in the database'
				END
				ELSE IF EXISTS (SELECT * FROM dbo.Person WHERE   Mail=@Mail)
					BEGIN 
						SET @RETURN=3
						SET @RETURN_MESSAGE='Your Mail already exists in the database'
					END
					ELSE IF EXISTS (SELECT * FROM dbo.Person WHERE  Phone=@Phone)
						BEGIN 
							SET @RETURN=4
							SET @RETURN_MESSAGE='Your Phone already exists in the database'
						END
GO
--=======================================================================================--
--==================PROCEDURE TO INSERT INTO INVENTORY TABLE=============================--

DROP PROCEDURE IF EXISTS InventoryInsert
GO

CREATE PROCEDURE InventoryInsert
	@Code VARCHAR (20),
	@Brand VARCHAR(20),
	@Color VARCHAR(20),
	@Quantity VARCHAR(20),
	@Price VARCHAR(20),
	@Preview IMAGE NULL,
	@RETURN TINYINT OUTPUT,
	@RETURN_MESSAGE VARCHAR(500) OUTPUT
AS
	IF NOT EXISTS(SELECT * FROM dbo.Inventory WHERE  Code=@Code)
		BEGIN 
			INSERT INTO [dbo].[Inventory]([Code],[Brand],[Color],[Quantity],[Price],[Preview])
			VALUES(@Code,@Brand,@Color,@Quantity,@Price,@Preview)
			SET @RETURN=0
			SET @RETURN_MESSAGE='Entered correctly'
		END
        ELSE IF EXISTS (SELECT * FROM dbo.Inventory WHERE   Code=@Code)
			BEGIN 
				SET @RETURN=1
				SET @RETURN_MESSAGE='Your Code already exists in the datebase'
			END
GO
--=======================================================================================--
--====================PROCEDURE TO INSERT INTO OPTOMETRY TABLE===========================--
DROP PROCEDURE IF EXISTS OptometryInsert
GO

CREATE PROCEDURE OptometryInsert
	@ID_Client TINYINT,
	@Eyes_Right VARCHAR(20),
	@Eyes_Left VARCHAR(20),
	@Distance VARCHAR(20),
	@Lenses_Type VARCHAR(20),
	@Medication VARCHAR(20),
	@Observation VARCHAR(500),
	@RETURN INT OUTPUT,
	@RETURN_MESSAGE VARCHAR(500) OUTPUT
AS
	IF NOT EXISTS (SELECT * FROM dbo.Client WHERE ID_Client = @ID_Client)
		BEGIN
			SET @RETURN = 1
			SET @RETURN_MESSAGE = 'Please Verify ID Client'
		END
		ELSE
			BEGIN
			-----------------------------------------------
				DECLARE @Exam_Date DATE
				SET @Exam_Date = GETDATE()
			-----------------------------------------------
				INSERT INTO dbo.Optometry([ID_Client],[Exam_Date],[Eyes_Right],[Eyes_Left],[Distance],[Lenses_Type],[Medication],[Observation])
				VALUES(@ID_Client,@Exam_Date,@Eyes_Right,@Eyes_Left,@Distance,@Lenses_Type,@Medication,@Observation)
			-----------------------------------------------				
				SET @RETURN = 0
				SET @RETURN_MESSAGE = 'Entered Correctly'
			END
GO
--=======================================================================================--
--====================PROCEDURE TO INSERT IN TABLE SALES=================================--
DROP PROCEDURE IF EXISTS SalesInsert
GO

CREATE PROCEDURE SalesInsert
	@ID_Client TINYINT,
	@Exam_Date DATE,
	@Medication VARCHAR(20),
	@Observation VARCHAR(500),
	@Code VARCHAR (20),
	@Brand VARCHAR(20),
	@Color VARCHAR(20),
	@RETURN INT OUTPUT,
	@RETURN_MESSAGE VARCHAR(500) OUTPUT
AS
	-----------------------------------------------
		DECLARE @ID TINYINT;
		DECLARE @Quantity INT;
	-----------------------------------------------
		SET @ID = (SELECT ID FROM dbo.Inventory WHERE Code = @Code)
		SET @Quantity = (SELECT Quantity FROM dbo.Inventory WHERE Code = @Code)
	-----------------------------------------------
		IF @Quantity>0
			BEGIN
				INSERT INTO dbo.Sales(ID_Client,Exam_Date,Medication,Observation,ID,Code,Brand,Color)
				VALUES(@ID_Client,@Exam_Date,@Medication,@Observation,@ID,@Code,@Brand,@Color)
	-----------------------------------------------			
				UPDATE dbo.Inventory 
					SET Quantity = Quantity - 1
					WHERE ID = @ID
	-----------------------------------------------
		SET @RETURN = 0;
		SET @RETURN_MESSAGE = 'Entered Correctly'
		END
		ELSE
			BEGIN
				SET @RETURN = 1;
				SET @RETURN_MESSAGE = 'There are no more products with this code'
			END	
GO
--=======================================================================================--