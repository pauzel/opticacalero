/*
	DATABASE OPTICA_CALERO
	SCHEDULED AND PREPARED BY:

	1. ALEJANDRO ANTONIO TELLEZ BARREDA
	2. MELVIN ANTONIO FLORES
	3. PAULINO JOSE ZELAYA TORU�O

	DATABASE PROJECT I
	NATIONAL UNIVERSITY OF ENGINEERING

	..:: ALL THE PROGRAMMING ARE ONLY AND EXCLUSIVELY
	PREPARED BY THE ABOVE MENTIONED PEOPLE ::..

*/
--=======================================================================================--
--============================CREATE OPTICA CALERO======================================--
CREATE DATABASE Optica_Calero;
GO
--=======================================================================================--
--============================USE OPTICA CALERO=======================================--
USE Optica_Calero;
GO
--=======================================================================================--
