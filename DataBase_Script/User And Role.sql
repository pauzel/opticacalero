/*
	DATABASE OPTICA_CALERO
	SCHEDULED AND PREPARED BY:

	1. ALEJANDRO ANTONIO TELLEZ BARREDA
	2. MELVIN ANTONIO FLORES
	3. PAULINO JOSE ZELAYA TORU�O

	DATABASE PROJECT I
	NATIONAL UNIVERSITY OF ENGINEERING

	..:: ALL THE PROGRAMMING ARE ONLY AND EXCLUSIVELY
	PREPARED BY THE ABOVE MENTIONED PEOPLE ::..

*/

/*
				>>>>>>>>>>	USER AND ROLE <<<<<<<<<<<<<
*/
--=======================================================================================--
USE MASTER
GO

CREATE LOGIN Optica WITH PASSWORD=N'1234'
GO

USE Optica_Calero
GO

CREATE USER Optica FOR LOGIN Optica
GO

--=======================================================================================--
CREATE ROLE db_select
go

ALTER ROLE db_select ADD MEMBER Optica
GO

GRANT SELECT ON SCHEMA::dbo TO db_select
GO
--=======================================================================================--
CREATE ROLE db_insert
GO

ALTER ROLE db_insert ADD MEMBER Optica
GO

GRANT INSERT ON SCHEMA::dbo TO db_insert
GO
--=======================================================================================--
CREATE ROLE db_update
go

ALTER ROLE db_update ADD MEMBER Optica
GO

GRANT UPDATE ON SCHEMA::dbo TO db_update
GO
--=======================================================================================--
CREATE ROLE db_delete
GO

ALTER ROLE db_delete ADD MEMBER Optica
GO

GRANT DELETE ON SCHEMA::dbo TO db_delete
GO
--=======================================================================================--
CREATE ROLE db_exec
GO

ALTER ROLE db_exec ADD MEMBER Optica

GRANT EXECUTE ON SCHEMA::dbo TO db_exec
GO  
--=======================================================================================--