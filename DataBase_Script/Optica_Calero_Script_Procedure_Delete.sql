/*
	DATABASE OPTICA_CALERO
	SCHEDULED AND PREPARED BY:

	1. ALEJANDRO ANTONIO TELLEZ BARREDA
	2. MELVIN ANTONIO FLORES
	3. PAULINO JOSE ZELAYA TORU�O

	DATABASE PROJECT I
	NATIONAL UNIVERSITY OF ENGINEERING

	..:: ALL THE PROGRAMMING ARE ONLY AND EXCLUSIVELY
	PREPARED BY THE ABOVE MENTIONED PEOPLE ::..

*/

/*
				>>>>>>>>>>	DELETE STORED PROCEDURES <<<<<<<<<<<<<
*/

--=======================================================================================--
--============================USE OPTICA CALERO=======================================--
USE Optica_Calero;
GO
--=======================================================================================--
--====================PROCEDURE DELETE FOR TABLE USER REGISTRATION========================--
DROP PROCEDURE IF EXISTS UserDelete
GO

CREATE PROCEDURE UserDelete
	@ID_Ced VARCHAR(20),
	@RETURN TINYINT OUTPUT,
	@RETURN_MESSAGE VARCHAR(500) OUTPUT
AS
	IF EXISTS(SELECT * FROM dbo.Person WHERE Ced = @ID_Ced)
		BEGIN
			DECLARE @ID_Person INT
			SET @ID_Person = (SELECT ID_Person FROM dbo.Person WHERE Ced = @ID_Ced) 

			DELETE FROM dbo.Users WHERE ID_Person = @ID_Person
			DELETE FROM dbo.Person WHERE ID_Person = @ID_Person

			SET @RETURN = 0
			SET @RETURN_MESSAGE = 'User Deleted Correctly'
		END
GO
--=======================================================================================--
--========================PROCEDURE DELETE FOR TABLE CLIENT==============================--
DROP PROCEDURE IF EXISTS ClientDelete
GO

CREATE PROCEDURE ClientDelete
	@ID_Ced VARCHAR(20),
	@RETURN TINYINT OUTPUT,
	@RETURN_MESSAGE VARCHAR(500) OUTPUT
AS
	IF EXISTS(SELECT * FROM dbo.Person WHERE Ced = @ID_Ced)
		BEGIN

			DECLARE @ID_Person INT
			SET @ID_Person = (SELECT ID_Person FROM dbo.Person WHERE Ced = @ID_Ced) 

			DELETE FROM dbo.Client WHERE ID_Person = @ID_Person
			DELETE FROM dbo.Person WHERE ID_Person = @ID_Person			 

			SET @RETURN = 0
			SET @RETURN_MESSAGE = 'User Deleted Correctly'
		END
GO
--=======================================================================================--
--=========================PROCEDURE DELETE TABLA INVENTORY==============================--
DROP PROCEDURE IF EXISTS InventoryDelete
GO

CREATE PROCEDURE InventoryDelete
	@ID VARCHAR(20),
	@RETURN TINYINT OUTPUT,
	@RETURN_MESSAGE VARCHAR(500) OUTPUT
AS
	IF EXISTS(SELECT * FROM dbo.Inventory WHERE Id = @Id)
		BEGIN
			DELETE FROM dbo.Inventory WHERE [ID] = @ID
			SET @RETURN = 0
			SET @RETURN_MESSAGE = 'User Deleted Correctly'
		END
GO
--=======================================================================================--