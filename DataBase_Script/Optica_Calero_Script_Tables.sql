/*
	DATABASE OPTICA_CALERO
	SCHEDULED AND PREPARED BY:

	1. ALEJANDRO ANTONIO TELLEZ BARREDA
	2. MELVIN ANTONIO FLORES
	3. PAULINO JOSE ZELAYA TORU�O

	DATABASE PROJECT I
	NATIONAL UNIVERSITY OF ENGINEERING

	..:: ALL THE PROGRAMMING ARE ONLY AND EXCLUSIVELY
	PREPARED BY THE ABOVE MENTIONED PEOPLE ::..

*/
--=======================================================================================--
------USAMOS OPTICA CALERO-----------------------------------------------------------------------
USE Optica_Calero;
-------------------------------------------------------------------------------------------------
------CREACION DE LA TABLA PERSONAS------------------------------------------------------
DROP TABLE IF EXISTS dbo.Person
GO

CREATE TABLE dbo.Person(
	[ID_Person] TINYINT IDENTITY(1,1),
	[Name] VARCHAR(20),
	[LastName] VARCHAR(20),
	[Phone] VARCHAR(20) NOT NULL,
	[Mail] VARCHAR(50) NOT NULL,
	[Direction] VARCHAR(30) NOT NULL,
	[Ced] VARCHAR(50) NOT NULL,
	PRIMARY KEY([ID_Person])
)
GO
-------------------------------------------------------------------------------------------------
------CREACION DE LA TABLA ADMIN Y EMPLEADO------------------------------------------------------
DROP TABLE IF EXISTS dbo.Users;
GO
CREATE TABLE dbo.Users(
	[ID_User] TINYINT IDENTITY(1,1) NOT NULL,
	[User] VARCHAR(20) NOT NULL,
	[Password] VARBINARY(100) NOT NULL DEFAULT ( HASHBYTES('SHA2_256','0x')),	
	[Department] VARCHAR(20) NOT NULL,
	[Municipality] VARCHAR(20) NOT NULL,
	[Role] VARCHAR(20) NOT NULL,
	[RecoveryCode] INT,
	[ID_Person] TINYINT,
	PRIMARY KEY ([ID_User]),
	FOREIGN KEY ([ID_Person]) REFERENCES dbo.Person ([ID_Person])
)
GO
-------------------------------------------------------------------------------------------------
------CREACION DE LA TABLA CLIENT------------------------------------------------------
DROP TABLE IF EXISTS dbo.Client;
GO
CREATE TABLE dbo.Client(
	[ID_Client] TINYINT IDENTITY(1,1) NOT NULL,
	[Age] TINYINT NOT NULL,
	[Ced] VARCHAR(50) NOT NULL,
	[ID_Person] TINYINT,
	PRIMARY KEY([ID_Client]),
	FOREIGN KEY ([ID_Person]) REFERENCES dbo.Person([ID_Person])
)
GO
-------------------------------------------------------------------------------------------------
------CREACION DE LA TABLA INVENTORY------------------------------------------------------
DROP TABLE IF EXISTS dbo.Inventory
GO
CREATE TABLE dbo.Inventory(
	[ID] TINYINT IDENTITY(1,1),
	[Code] VARCHAR (20),
	[Brand] VARCHAR(20),
	[Color] VARCHAR(20),
	[Quantity] VARCHAR(20),
	[Price] VARCHAR(20),
	[Preview] IMAGE NULL,
	PRIMARY KEY ([ID])
)
GO
-------------------------------------------------------------------------------------------------
------CREACION DE LA TABLA OPTOMETRY------------------------------------------------------
DROP TABLE IF EXISTS dbo.Optometry
GO
CREATE TABLE dbo.Optometry(
	[ID_Client] TINYINT,
	[Exam_Date] DATE,
	[Eyes_Right] VARCHAR(20),
	[Eyes_Left] VARCHAR(20),
	[Distance] VARCHAR(20),
	[Lenses_Type] VARCHAR(20),
	[Medication] VARCHAR(20),
	[Observation] VARCHAR(500),
	FOREIGN KEY ([ID_Client]) REFERENCES dbo.Client([ID_Client])
)
GO
---------------------------------------------------------------------------------------------------
------CREACION DE LA TABLA SALES------------------------------------------------------
DROP TABLE IF EXISTS dbo.Sales
GO

CREATE TABLE dbo.Sales(
	[ID_Sales] TINYINT IDENTITY(1,1),
	[ID_Client] TINYINT,
	[Exam_Date] DATE,
	[Medication] VARCHAR(20),
	[Observation] VARCHAR(500),
	[ID] TINYINT,
	[Code] VARCHAR (20),
	[Brand] VARCHAR(20),
	[Color] VARCHAR(20),
	PRIMARY KEY ([ID_Sales]),
	FOREIGN KEY ([ID_Client]) REFERENCES dbo.Client([ID_Client]),
	FOREIGN KEY ([ID]) REFERENCES dbo.Inventory([ID])
)
GO
------------------------------------------------------------------------
SELECT * FROM dbo.Sales