package View;

import ConnectionSql.ConnectionSQLServer;
import Controller.LoginController;
import Model.LoginModel;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import paneles.RecoveryPassword;

public class LoginForm extends javax.swing.JFrame {

    private LoginController LC;

    public LoginForm() {
        initComponents();
        setController();
        

        this.setLocationRelativeTo(this);
        this.ExitButton.setCursor(new Cursor(HAND_CURSOR));
        this.LoginButton.setCursor(new Cursor(HAND_CURSOR));
        this.jLabel6.setCursor(new Cursor(HAND_CURSOR));
        this.VisibleCheckBox.setCursor(new Cursor(HAND_CURSOR));

        setIconImage(new ImageIcon(getClass().getResource("/resources/logoventana.png")).getImage());
    }
    
    public void Clean(){
        userTextField.setText("");
        passwordTextField.setText("");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        ExitButton = new javax.swing.JButton();
        VisibleCheckBox = new javax.swing.JCheckBox();
        LoginButton = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        passwordTextField = new javax.swing.JPasswordField();
        jSeparator1 = new javax.swing.JSeparator();
        userTextField = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        error = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel6.setFont(new java.awt.Font("Berlin Sans FB Demi", 0, 12)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(51, 153, 255));
        jLabel6.setText("Forgot your password?");
        jLabel6.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                jLabel6MouseMoved(evt);
            }
        });
        jLabel6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel6MouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel6MouseExited(evt);
            }
        });
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 330, 130, 20));

        ExitButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/exit.png"))); // NOI18N
        ExitButton.setToolTipText("Exit");
        ExitButton.setActionCommand("ExitButton");
        ExitButton.setBorder(null);
        ExitButton.setContentAreaFilled(false);
        ExitButton.setDefaultCapable(false);
        ExitButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ExitButtonActionPerformed(evt);
            }
        });
        jPanel1.add(ExitButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 10, 30, 20));

        VisibleCheckBox.setForeground(new java.awt.Color(255, 255, 255));
        VisibleCheckBox.setToolTipText("Show Password");
        VisibleCheckBox.setActionCommand("ShowPassword");
        VisibleCheckBox.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/EnableEye.png"))); // NOI18N
        VisibleCheckBox.setOpaque(false);
        VisibleCheckBox.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                VisibleCheckBoxMouseClicked(evt);
            }
        });
        VisibleCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                VisibleCheckBoxActionPerformed(evt);
            }
        });
        jPanel1.add(VisibleCheckBox, new org.netbeans.lib.awtextra.AbsoluteConstraints(284, 210, 30, 30));

        LoginButton.setFont(new java.awt.Font("Berlin Sans FB Demi", 0, 14)); // NOI18N
        LoginButton.setForeground(new java.awt.Color(255, 255, 255));
        LoginButton.setText("Log in");
        LoginButton.setActionCommand("Login");
        LoginButton.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255), 2));
        LoginButton.setContentAreaFilled(false);
        LoginButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LoginButtonActionPerformed(evt);
            }
        });
        jPanel1.add(LoginButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 290, 70, 30));
        jPanel1.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 240, 270, 10));

        passwordTextField.setBackground(new java.awt.Color(39, 6, 82));
        passwordTextField.setFont(new java.awt.Font("Berlin Sans FB Demi", 0, 14)); // NOI18N
        passwordTextField.setForeground(new java.awt.Color(153, 255, 255));
        passwordTextField.setBorder(null);
        passwordTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                passwordTextFieldKeyPressed(evt);
            }
        });
        jPanel1.add(passwordTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 210, 270, 30));
        jPanel1.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 170, 270, 10));

        userTextField.setBackground(new java.awt.Color(39, 6, 82));
        userTextField.setFont(new java.awt.Font("Berlin Sans FB Demi", 0, 14)); // NOI18N
        userTextField.setForeground(new java.awt.Color(153, 255, 255));
        userTextField.setBorder(null);
        userTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                userTextFieldKeyPressed(evt);
            }
        });
        jPanel1.add(userTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 140, 270, 30));

        jLabel4.setFont(new java.awt.Font("Berlin Sans FB Demi", 0, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Password");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 190, 80, 20));

        jLabel5.setFont(new java.awt.Font("Berlin Sans FB Demi", 0, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("User");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 120, 50, 20));

        jLabel2.setFont(new java.awt.Font("Berlin Sans FB", 0, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Optica Calero");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 50, -1, -1));

        error.setForeground(new java.awt.Color(255, 51, 51));
        error.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jPanel1.add(error, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 80, 330, 20));

        jLabel1.setBackground(new java.awt.Color(255, 0, 0));
        jLabel1.setForeground(new java.awt.Color(255, 0, 51));
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/fondologin.jpg"))); // NOI18N
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 330, 460));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void VisibleCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_VisibleCheckBoxActionPerformed

    }//GEN-LAST:event_VisibleCheckBoxActionPerformed

    private void ExitButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ExitButtonActionPerformed
        dispose();
    }//GEN-LAST:event_ExitButtonActionPerformed

    private void jLabel6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel6MouseClicked
        RecoveryPassword rp = new RecoveryPassword();
        rp.setVisible(true);
    }//GEN-LAST:event_jLabel6MouseClicked

    private void jLabel6MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel6MouseExited
        this.jLabel6.setForeground(new Color(51, 153, 255));
    }//GEN-LAST:event_jLabel6MouseExited

    private void jLabel6MouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel6MouseMoved
        this.jLabel6.setForeground(new Color(224, 255, 255));
    }//GEN-LAST:event_jLabel6MouseMoved

    private void LoginButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LoginButtonActionPerformed
      
    }//GEN-LAST:event_LoginButtonActionPerformed

    private void passwordTextFieldKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_passwordTextFieldKeyPressed
         if(evt.getKeyCode()==KeyEvent.VK_ENTER){
             LC.login();
         }
    }//GEN-LAST:event_passwordTextFieldKeyPressed

    private void userTextFieldKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_userTextFieldKeyPressed
        if(evt.getKeyCode()==KeyEvent.VK_ENTER){
            LC.login();
        }
    }//GEN-LAST:event_userTextFieldKeyPressed

    private void VisibleCheckBoxMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_VisibleCheckBoxMouseClicked
     
        if (VisibleCheckBox.isSelected()) {
            //CAPTURA LO ESCRITO Y LO MUESTA TIPO STRING
            VisibleCheckBox.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/resources/DisableEye.png")));  
            this.passwordTextField.setEchoChar((char) 0);
        } else {
            //CAPTURA LO ESCRITO Y LO MUESTR CON 
            VisibleCheckBox.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/resources/EnableEye.png")));  
            this.passwordTextField.setEchoChar('*');
        }
    }//GEN-LAST:event_VisibleCheckBoxMouseClicked

    private void setController() {
        LC = new LoginController(this);
        LoginButton.addActionListener(LC);
    }

    public LoginModel GetData() {
        LoginModel LM = new LoginModel();
        LM.setUser(userTextField.getText());
        LM.setPassword(String.valueOf(passwordTextField.getPassword()));
        return LM;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(LoginForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(LoginForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(LoginForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(LoginForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new LoginForm().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton ExitButton;
    private javax.swing.JButton LoginButton;
    private javax.swing.JCheckBox VisibleCheckBox;
    public javax.swing.JLabel error;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    public javax.swing.JPasswordField passwordTextField;
    public javax.swing.JTextField userTextField;
    // End of variables declaration//GEN-END:variables

}
