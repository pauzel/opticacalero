package Misc;

import javax.swing.JTable;

public class DesignTable {

    HeaderColorRow HCR = new HeaderColorRow();
    HeaderColorTitle HCT = new HeaderColorTitle();

    public void DesignTable(JTable mytable) {
        mytable.getTableHeader().setDefaultRenderer(HCT);
        
        for (int i = 0; i < mytable.getColumnCount(); i++) {
            mytable.getColumnModel().getColumn(i).setCellRenderer(HCR);
        }
    }
}
