package Misc;

import Misc.DesignTable;
import Misc.HeaderColorTitle;
import Misc.InventoryProcess;
import Model.InventoryModel;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;


public class Show_Tabla_Producto{

   InventoryProcess dao = null;
   HeaderColorTitle HCT = new HeaderColorTitle();


    public void visualizar_ProductoVO(JTable tabla){
        tabla.getTableHeader().setDefaultRenderer(HCT);
        tabla.setDefaultRenderer(Object.class, new Render());
        DefaultTableModel dt = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int row, int column){
                return false;
            }
        };
        dt.addColumn("ID");
        dt.addColumn("Codigo");
        dt.addColumn("Brand");
        dt.addColumn("Color");
        dt.addColumn("Quantity");
        dt.addColumn("Price");
        dt.addColumn("Preview");
        

        dao = new InventoryProcess();
        InventoryModel vo = new InventoryModel();
        ArrayList<InventoryModel> list = dao.Listar_ProductoVO();

        if(list.size() > 0){
            for(int i=0; i<list.size(); i++){
                Object fila[] = new Object[7];
                vo = list.get(i);
                fila[0] = vo.getId();
                fila[1] = vo.getCode();
                fila[2] = vo.getBrand();
                fila[3] = vo.getColor();
                fila[4] = vo.getQuantity();
                fila[5] = vo.getPrice();
                try{
                    byte[] bi = vo.getPreview();
                    BufferedImage image = null;
                    InputStream in = new ByteArrayInputStream(bi);
                    image = ImageIO.read(in);
                    ImageIcon imgi = new ImageIcon(image.getScaledInstance(60, 60, 0));
                    
                    fila[6] = new JLabel(imgi);

                }catch(Exception ex){
                    
                    fila[6] = new JLabel("No imagen");
                }
                dt.addRow(fila);
            }
            tabla.setModel(dt);
            tabla.setRowHeight(60);
        }
    }
    
    
}