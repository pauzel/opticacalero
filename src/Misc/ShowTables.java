package Misc;

import ConnectionSql.ConnectionSQLServer;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;
import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

public class ShowTables {

    ConnectionSQLServer cc = new ConnectionSQLServer();
    Connection con = cc.getConnection();
    DesignTable DT = new DesignTable();

    public void OptometrySearchTable(JTable tabla, String code) {
        DT.DesignTable(tabla);
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        TableRowSorter<TableModel> ordenamiento = new TableRowSorter<TableModel>(model);
        tabla.setRowSorter(ordenamiento);
        model.setRowCount(0);

        try {
            String sql = "EXEC OptometrySearch ?";
            ResultSet rs = null;
            PreparedStatement ps = null;

            ps = cc.getConnection().prepareStatement(sql);
            ps.setString(1, code);
            rs = ps.executeQuery();

            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getInt(1));
                v.add(rs.getString(2));
                v.add(rs.getString(3));
                v.add(rs.getString(4));
                v.add(rs.getString(5));
                v.add(rs.getString(6));
                v.add(rs.getString(7));
                v.add(rs.getString(8));
                model.addRow(v);

                tabla.setModel(model);
            }

        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }

    }

    public void LoadOptometryTable(JTable tabla) {
        DT.DesignTable(tabla);
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        TableRowSorter<TableModel> ordenamiento = new TableRowSorter<TableModel>(model);
        tabla.setRowSorter(ordenamiento);
        model.setRowCount(0);

        try {
            String sql = "SELECT * FROM dbo.Optometry";
            ResultSet rs = null;
            PreparedStatement ps = null;

            ps = cc.getConnection().prepareStatement(sql);
            rs = ps.executeQuery();

            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getInt(1));
                v.add(rs.getString(2));
                v.add(rs.getString(3));
                v.add(rs.getString(4));
                v.add(rs.getString(5));
                v.add(rs.getString(6));
                v.add(rs.getString(7));
                v.add(rs.getString(8));
                model.addRow(v);

                tabla.setModel(model);
            }

        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }

    }

    public void SalesSearchTable(JTable tabla, String code) {
        DT.DesignTable(tabla);
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        TableRowSorter<TableModel> ordenamiento = new TableRowSorter<TableModel>(model);
        tabla.setRowSorter(ordenamiento);
        model.setRowCount(0);

        try {
            String sql = "EXEC SalesSearch ?";
            ResultSet rs = null;
            PreparedStatement ps = null;

            ps = cc.getConnection().prepareStatement(sql);
            ps.setString(1, code);
            rs = ps.executeQuery();

            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getInt(2));
                v.add(rs.getString(3));
                v.add(rs.getString(4));
                v.add(rs.getString(5));
                v.add(rs.getString(7));
                v.add(rs.getString(8));
                v.add(rs.getString(9));
                model.addRow(v);

                tabla.setModel(model);
            }

        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }

    }

    public void LoadSalesTable(JTable tabla) {
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        TableRowSorter<TableModel> ordenamiento = new TableRowSorter<TableModel>(model);
        tabla.setRowSorter(ordenamiento);
        model.setRowCount(0);

        try {
            String sql = "SELECT * FROM dbo.Sales";
            ResultSet rs = null;
            PreparedStatement ps = null;

            ps = cc.getConnection().prepareStatement(sql);
            rs = ps.executeQuery();

            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getInt(2));
                v.add(rs.getString(3));
                v.add(rs.getString(4));
                v.add(rs.getString(5));
                v.add(rs.getString(7));
                v.add(rs.getString(8));
                v.add(rs.getString(9));
                model.addRow(v);

                tabla.setModel(model);
            }

        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }

    }

    public void ClientSearchTable(JTable tabla, String ced) {
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        TableRowSorter<TableModel> ordenamiento = new TableRowSorter<TableModel>(model);
        tabla.setRowSorter(ordenamiento);
        model.setRowCount(0);

        try {
            String sql = "EXEC ClientSearch ?";
            ResultSet rs = null;
            PreparedStatement ps = null;

            ps = cc.getConnection().prepareStatement(sql);
            ps.setString(1, ced);
            rs = ps.executeQuery();

            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getInt(1));
                v.add(rs.getString(2));
                v.add(rs.getString(3));
                v.add(rs.getString(4));
                v.add(rs.getString(5));
                v.add(rs.getString(6));
                v.add(rs.getString(7));
                v.add(rs.getString(8));
                model.addRow(v);
                tabla.setModel(model);
            }

        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }
    }

    public void LoadClientTable(JTable tabla) {
        DT.DesignTable(tabla);
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        TableRowSorter<TableModel> ordenamiento = new TableRowSorter<TableModel>(model);
        tabla.setRowSorter(ordenamiento);
        model.setRowCount(0);

        try {
            String sql = "SELECT * FROM ViewClients";
            ResultSet rs = null;
            PreparedStatement ps = null;

            ps = cc.getConnection().prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getInt(1));
                v.add(rs.getString(2));
                v.add(rs.getString(3));
                v.add(rs.getString(4));
                v.add(rs.getString(5));
                v.add(rs.getString(6));
                v.add(rs.getString(7));
                v.add(rs.getString(8));
                model.addRow(v);
                tabla.setModel(model);
            }

        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }

    }

    public void SearchInventory(JTable tabla, String code) {
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        TableRowSorter<TableModel> ordenamiento = new TableRowSorter<TableModel>(model);
        tabla.setRowSorter(ordenamiento);
        model.setRowCount(0);

        try {
            String sql = "EXEC InventorySearch ?";
            ResultSet rs = null;
            PreparedStatement ps = null;

            ps = cc.getConnection().prepareStatement(sql);
            ps.setString(1, code);
            rs = ps.executeQuery();
            while (rs.next()) {

                Vector v = new Vector();
                v.add(rs.getInt(1));
                v.add(rs.getString(2));
                v.add(rs.getString(3));
                v.add(rs.getString(4));
                v.add(rs.getString(5));
                v.add(rs.getString(6));
                try {
                    byte[] bi = rs.getBytes("Preview");
                    BufferedImage image = null;
                    InputStream in = new ByteArrayInputStream(bi);
                    image = ImageIO.read(in);
                    ImageIcon imgi = new ImageIcon(image.getScaledInstance(60, 60, 0));

                    v.add(new JLabel(imgi));

                } catch (Exception ex) {

                    v.add(new JLabel("No image"));
                }
                model.addRow(v);
                tabla.setModel(model);
            }

        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }
    }

    public void SearchUserTable(JTable tabla, String ced) {
        DT.DesignTable(tabla);
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        TableRowSorter<TableModel> ordenamiento = new TableRowSorter<TableModel>(model);
        tabla.setRowSorter(ordenamiento);
        model.setRowCount(0);

        try {
            String sql = "EXEC UserSearch ?";
            ResultSet rs = null;
            PreparedStatement ps = null;

            ps = cc.getConnection().prepareStatement(sql);
            ps.setString(1, ced);
            rs = ps.executeQuery();

            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getInt(1));
                v.add(rs.getString(2));
                v.add(rs.getString(3));
                v.add(rs.getString(4));
                v.add(rs.getString(5));
                v.add(rs.getString(6));
                v.add(rs.getString(7));
                v.add(rs.getString(8));
                v.add(rs.getString(9));
                v.add(rs.getString(10));
                v.add(rs.getString(11));
                v.add(rs.getString(12));
                v.add(rs.getString(13));
                model.addRow(v);

                tabla.setModel(model);
            }

        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }
    }

    public void LoadUserTable(JTable tabla) {
        DT.DesignTable(tabla);
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        TableRowSorter<TableModel> ordenamiento = new TableRowSorter<TableModel>(model);
        tabla.setRowSorter(ordenamiento);
        model.setRowCount(0);

        try {
            String sql = "SELECT * FROM ViewUsers";
            ResultSet rs = null;
            PreparedStatement ps = null;

            ps = cc.getConnection().prepareStatement(sql);
            rs = ps.executeQuery();
            
            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getInt(1));
                v.add(rs.getString(2));
                v.add(rs.getString(3));
                v.add(rs.getString(4));
                v.add(rs.getString(5));
                v.add(rs.getString(6));
                v.add(rs.getString(7));
                v.add(rs.getString(8));
                v.add(rs.getString(9));
                v.add(rs.getString(10));
                v.add(rs.getString(11));
                v.add(rs.getString(12));
                v.add(rs.getString(13));
                model.addRow(v);

                tabla.setModel(model);
            }

        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }

    }

    public void LoadTableSalesRecord(JTable tabla) {
        DT.DesignTable(tabla);
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        TableRowSorter<TableModel> ordenamiento = new TableRowSorter<TableModel>(model);
        tabla.setRowSorter(ordenamiento);
        model.setRowCount(0);

        try {
            String sql = "SELECT * FROM SalesRecord";
            ResultSet rs = null;
            PreparedStatement ps = null;

            ps = cc.getConnection().prepareStatement(sql);
            rs = ps.executeQuery();

            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getInt(1));
                v.add(rs.getString(2));
                v.add(rs.getString(3));
                v.add(rs.getString(4));
                v.add(rs.getString(5));
                v.add(rs.getString(6));
                v.add(rs.getString(7));
                model.addRow(v);

                tabla.setModel(model);
            }

        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }

    }
}
