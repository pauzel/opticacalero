package Misc;

import ConnectionSql.ConnectionSQLServer;
import Model.InventoryModel;
import java.sql.*;
import java.util.ArrayList;


/*Metodo listar*/
public class InventoryProcess {

    ConnectionSQLServer conec = new ConnectionSQLServer();

    public ArrayList<InventoryModel> Listar_ProductoVO() {
        ArrayList<InventoryModel> list = new ArrayList<InventoryModel>();

        String sql = "SELECT * FROM Inventory;";
        ResultSet rs = null;
        PreparedStatement ps = null;
        try {
            ps = conec.getConnection().prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                InventoryModel vo = new InventoryModel();
                vo.setId(rs.getString(1));
                vo.setCode(rs.getString(2));
                vo.setBrand(rs.getString(3));
                vo.setColor(rs.getString(4));
                vo.setQuantity(rs.getString(5));
                vo.setPrice(rs.getString(6));
                vo.setPreview(rs.getBytes(7));
                list.add(vo);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            try {
                // ps.close();
                //rs.close();
                // conec.desconectar();
            } catch (Exception ex) {
            }
        }
        return list;
    }
    
    


    /*Metodo agregar*/
    public void Agregar_ProductoVO(InventoryModel vo) {
        String sql = "INSERT INTO Inventory (Code, Brand, Color, Quantity,Price,Preview)\n"
                + "VALUES (?,?,?,?,?,?);";
        PreparedStatement ps = null;
        try {
            ps = conec.getConnection().prepareStatement(sql);
            ps.setString(1, vo.getCode());
            ps.setString(2, vo.getBrand());
            ps.setString(3, vo.getColor());
            ps.setString(4, vo.getQuantity());
            ps.setString(5, vo.getPrice());
            ps.setBytes(6, vo.getPreview());
            ps.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("A " + ex.getMessage());
        } catch (Exception ex) {
            System.out.println("B " + ex.getMessage());
        }
    }

    public void Modificar_ProductoVO(InventoryModel vo) {
        //Conectar conec = new Conectar();
        String sql = "UPDATE Inventory SET Code = ?, Brand = ?, Color = ?, Quantity = ?,Price = ?,Preview = ?\n"
                + "WHERE ID = ?;";
        PreparedStatement ps = null;
        try {
            ps = conec.getConnection().prepareStatement(sql);
            ps.setString(1, vo.getCode());
            ps.setString(2, vo.getBrand());
            ps.setString(3, vo.getColor());
            ps.setString(4, vo.getQuantity());
            ps.setString(5, vo.getPrice());
            ps.setBytes(6, vo.getPreview());
            ps.setString(7, vo.getId());
            ps.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            try {
                ps.close();
                conec.desconectar();
            } catch (Exception ex) {
            }
        }
    }

    public void Modificar_ProductoVO2(InventoryModel vo) {
        //Conectar conec = new Conectar();
        String sql = "UPDATE Inventory SET Code = ?, Brand = ?, Color = ?, Quantity = ?,Price = ?\n"
                + "WHERE ID = ?;";
        PreparedStatement ps = null;
        try {
            ps = conec.getConnection().prepareStatement(sql);
            ps.setString(1, vo.getCode());
            ps.setString(2, vo.getBrand());
            ps.setString(3, vo.getColor());
            ps.setString(4, vo.getQuantity());
            ps.setString(5, vo.getPrice());
           // ps.setBytes(6, vo.getPreview());
            ps.setString(6, vo.getId());
            ps.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            try {
                ps.close();
                conec.desconectar();
            } catch (Exception ex) {
            }
        }
    }

    public void Eliminar_ProductoVO(InventoryModel vo) {
        String sql = "DELETE FROM Inventory WHERE ID = ?";
        PreparedStatement ps = null;
        try {
            ps = conec.getConnection().prepareStatement(sql);
            ps.setString(1, vo.getId());
            ps.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            try {
                ps.close();
                conec.desconectar();
            } catch (Exception ex) {
            }
        }
    }

}
