package Model;

public class RecoveryPasswordModel {
    String mail;
    String recoverycode;
    String newpassword;
    String repeatpassword;

    public RecoveryPasswordModel(String mail, String recoverycode,String newpassword,String repeatpassword) {
        this.mail = mail;
        this.recoverycode = recoverycode;
        this.newpassword = newpassword;
        this.repeatpassword = repeatpassword;
    }
    
    public RecoveryPasswordModel() {
        this.mail = "";
        this.recoverycode = "";
        this.newpassword = "";
        this.repeatpassword = "";
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getRecoverycode() {
        return recoverycode;
    }

    public void setRecoverycode(String recoverycode) {
        this.recoverycode = recoverycode;
    }

    public String getNewpassword() {
        return newpassword;
    }

    public void setNewpassword(String newpassword) {
        this.newpassword = newpassword;
    }

    public String getRepeatpassword() {
        return repeatpassword;
    }

    public void setRepeatpassword(String repeatpassword) {
        this.repeatpassword = repeatpassword;
    }
    
    
}
