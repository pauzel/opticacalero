package Model;

import java.io.Serializable;

public class InventoryModel implements Serializable {

    String Id;
    String Code;
    String Brand;
    String Color;
    String Quantity;
    String price;
    byte[] Preview;

    public InventoryModel(String Id, String Code, String Brand, String Color, String Quantity, String price, byte[] Preview) {
        this.Id = Id;
        this.Code = Code;
        this.Brand = Brand;
        this.Color = Color;
        this.Quantity = Quantity;
        this.price = price;
        this.Preview = Preview;
    }

    public InventoryModel() {}

    public String getId() {
        return Id;
    }

    public void setId(String Id) {
        this.Id = Id;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public String getBrand() {
        return Brand;
    }

    public void setBrand(String Brand) {
        this.Brand = Brand;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String Color) {
        this.Color = Color;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String Quantity) {
        this.Quantity = Quantity;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public byte[] getPreview() {
        return Preview;
    }

    public void setPreview(byte[] Preview) {
        this.Preview = Preview;
    }
    
    
}
