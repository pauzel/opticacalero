package Model;

public class SalesRecordModel {

    String ID_Sales;

    public SalesRecordModel(String ID_Sales) {
        this.ID_Sales = ID_Sales;
    }

    public SalesRecordModel() {
        this.ID_Sales = "";
    }

    public String getID_Sales() {
        return ID_Sales;
    }

    public void setID_Sales(String ID_Sales) {
        this.ID_Sales = ID_Sales;
    }
}
