package Model;

import java.io.Serializable;

public class ClientModel implements Serializable{
    String ID;
    String Ced;
    String Name;
    String LastName;
    String Age;
    String Direction;
    String Mail;
    String Phone;

    public ClientModel(String ID, String Ced, String Name, String LastName, String Age, String Direction, String Mail, String Phone) {
        this.ID = ID;
        this.Ced = Ced;
        this.Name = Name;
        this.LastName = LastName;
        this.Age = Age;
        this.Direction = Direction;
        this.Mail = Mail;
        this.Phone = Phone;
    }
    
    public ClientModel(){
        this.ID = "";
        this.Ced = "";
        this.Name = "";
        this.LastName = "";
        this.Age = "";
        this.Direction = "";
        this.Mail = "";
        this.Phone = "";
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getCed() {
        return Ced;
    }

    public void setCed(String Ced) {
        this.Ced = Ced;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String LastName) {
        this.LastName = LastName;
    }

    public String getAge() {
        return Age;
    }

    public void setAge(String Age) {
        this.Age = Age;
    }

    public String getDirection() {
        return Direction;
    }

    public void setDirection(String Direction) {
        this.Direction = Direction;
    }

    public String getMail() {
        return Mail;
    }

    public void setMail(String Mail) {
        this.Mail = Mail;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String Phone) {
        this.Phone = Phone;
    }
}
