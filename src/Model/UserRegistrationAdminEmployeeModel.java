package Model;

import java.io.Serializable;

public class UserRegistrationAdminEmployeeModel implements Serializable{
    String ID_User;
    String ID_Ced;
    String User;
    String Password;
    String Name;
    String LastName;
    String Phone;
    String Mail;
    String Department;
    String Municipality;
    String Role;
    String Direction;
    String RecoveryCode;

    public UserRegistrationAdminEmployeeModel(String ID_User, String ID_Ced, String User, String Password, String Name, String LastName, String Phone, String Mail, String Department, String Municipality, String Role, String Direction, String RecoveryCode) {
        this.ID_User = ID_User;
        this.ID_Ced = ID_Ced;
        this.User = User;
        this.Password = Password;
        this.Name = Name;
        this.LastName = LastName;
        this.Phone = Phone;
        this.Mail = Mail;
        this.Department = Department;
        this.Municipality = Municipality;
        this.Role = Role;
        this.Direction = Direction;
        this.RecoveryCode = RecoveryCode;
    }
    
    public UserRegistrationAdminEmployeeModel() {
        this.ID_User = "";
        this.ID_Ced = "";
        this.User = "";
        this.Password = "";
        this.Name = "";
        this.LastName = "";
        this.Phone = "";
        this.Mail = "";
        this.Department = "";
        this.Municipality = "";
        this.Role = "";
        this.Direction = "";
        this.RecoveryCode = "";
    }

    public String getID_User() {
        return ID_User;
    }

    public void setID_User(String ID_User) {
        this.ID_User = ID_User;
    }

    public String getID_Ced() {
        return ID_Ced;
    }

    public void setID_Ced(String ID_Ced) {
        this.ID_Ced = ID_Ced;
    }

    public String getUser() {
        return User;
    }

    public void setUser(String User) {
        this.User = User;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String LastName) {
        this.LastName = LastName;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String Phone) {
        this.Phone = Phone;
    }

    public String getMail() {
        return Mail;
    }

    public void setMail(String Mail) {
        this.Mail = Mail;
    }

    public String getDepartment() {
        return Department;
    }

    public void setDepartment(String Department) {
        this.Department = Department;
    }

    public String getMunicipality() {
        return Municipality;
    }

    public void setMunicipality(String Municipality) {
        this.Municipality = Municipality;
    }

    public String getRole() {
        return Role;
    }

    public void setRole(String Role) {
        this.Role = Role;
    }

    public String getDirection() {
        return Direction;
    }

    public void setDirection(String Direction) {
        this.Direction = Direction;
    }
    
    public String getRecvoeryCode() {
        return Direction;
    }

    public void setRecoveryCode(String RecoveryCode) {
        this.RecoveryCode = RecoveryCode;
    }   
}