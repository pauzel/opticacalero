package Model;

import java.io.Serializable;

public class OptometryModel implements Serializable {

    String ID_Cliente;
    String Exam_Date;
    String Eyes_Right;
    String Eyes_Left;
    String Distance;
    String Lenses_Type;
    String Medication;
    String Observation;

    public OptometryModel(String ID_Cliente, String Exam_Date, String Eyes_Right, String Eyes_Left, String Distance, String Lenses_Type, String Medication, String Observation) {
        this.ID_Cliente = ID_Cliente;
        this.Exam_Date = Exam_Date;
        this.Eyes_Right = Eyes_Right;
        this.Eyes_Left = Eyes_Left;
        this.Distance = Distance;
        this.Lenses_Type = Lenses_Type;
        this.Medication = Medication;
        this.Observation = Observation;
    }

    public OptometryModel() {
        this.ID_Cliente = "";
        this.Exam_Date = "";
        this.Eyes_Right = "";
        this.Eyes_Left = "";
        this.Distance = "";
        this.Lenses_Type = "";
        this.Medication = "";
        this.Observation = "";
    }

    public String getID_Cliente() {
        return ID_Cliente;
    }

    public void setID_Cliente(String ID_Cliente) {
        this.ID_Cliente = ID_Cliente;
    }

    public String getExam_Date() {
        return Exam_Date;
    }

    public void setExam_Date(String Exam_Date) {
        this.Exam_Date = Exam_Date;
    }

    public String getEyes_Right() {
        return Eyes_Right;
    }

    public void setEyes_Right(String Eyes_Right) {
        this.Eyes_Right = Eyes_Right;
    }

    public String getEyes_Left() {
        return Eyes_Left;
    }

    public void setEyes_Left(String Eyes_Left) {
        this.Eyes_Left = Eyes_Left;
    }

    public String getDistance() {
        return Distance;
    }

    public void setDistance(String Distance) {
        this.Distance = Distance;
    }

    public String getLenses_Type() {
        return Lenses_Type;
    }

    public void setLenses_Type(String Lenses_Type) {
        this.Lenses_Type = Lenses_Type;
    }

    public String getMedication() {
        return Medication;
    }

    public void setMedication(String Medication) {
        this.Medication = Medication;
    }

    public String getObservation() {
        return Observation;
    }

    public void setObservation(String Observation) {
        this.Observation = Observation;
    }
}