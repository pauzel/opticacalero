package Model;

import java.io.Serializable;

public class LoginModel implements Serializable{
    String User;
    String Password;

    public LoginModel(String User, String Password) {
        this.User = User;
        this.Password = Password;
    }
    
    public LoginModel() {
        this.User = "";
        this.Password = "";
    } 

    public String getUser() {
        return User;
    }

    public void setUser(String User) {
        this.User = User;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }   
}
