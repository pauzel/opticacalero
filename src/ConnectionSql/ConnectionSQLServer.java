package ConnectionSql;

import java.sql.*;
import javax.swing.JOptionPane;

public class ConnectionSQLServer {

    static String bd = "Optica_Calero";
    static String user = "Optica";
    static String password = "1234";
    static String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
    static String url = "jdbc:sqlserver://localhost:1433;databaseName=" + bd + ";integratedSecurity=false";
    Connection connection = null;

    public ConnectionSQLServer() {
        try {
            Class.forName(driver);
            connection = DriverManager.getConnection(url, user, password);
            if (connection != null) {
                //System.out.println("The Connection at databse " + bd + " is complete");
            }
        } catch (SQLException | ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public Connection getConnection() {
        return connection;
    }

    public void desconectar() {
        try {
            System.out.println("Closing Connection");
            connection.close();
        } catch (Exception ex) {
        }
    }

    /*public static void main(String[] args) {
        ConnectionSQLServer C = new ConnectionSQLServer();
        C.getConnection();
    }*/
}
