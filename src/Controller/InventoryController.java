package Controller;

import ConnectionSql.ConnectionSQLServer;
import Misc.InventoryProcess;
import Model.InventoryModel;
import Misc.Show_Tabla_Producto;
import View.AdminForm;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import paneles.inventoryPanel;
import java.sql.*;
import javax.swing.JOptionPane;
import java.sql.Blob;
import javax.swing.JTable;

public class InventoryController implements ActionListener {

    InventoryProcess dao;
    Show_Tabla_Producto t = new Show_Tabla_Producto();
    InventoryModel IM;
    inventoryPanel IP;
    ConnectionSQLServer cc = new ConnectionSQLServer();
    Connection con = cc.getConnection();

    public InventoryController(inventoryPanel IP) {
        super();
        IM = new InventoryModel();
        this.IP = IP;
        // vo.visualizar_ProductoVO(IP.InventoryTable);
    }

    public InventoryModel getIM() {
        return IM;
    }

    public void setIM(InventoryModel IM) {
        this.IM = IM;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Insert":
                Insert();
                IP.clean();
                break;
            case "Modify":
                Update();
                IP.clean();
                break;
            case "Delete":
                int s = JOptionPane.showConfirmDialog(null, "¿Esta seguro de eliminar?", "Si/No", 0);
                    if (s == 0) {
                        Delete();
                        IP.clean();
                    }else{
                        IP.clean();
                    }
                
                break;
            case "Image":
                Image();
                break;
        }
    }

    public void Insert() {

        IM = IP.getInventoryData();
        String Id = IM.getId();
        String Code = IM.getCode();
        String Brand = IM.getBrand();
        String Color = IM.getColor();
        String Quantity = IM.getQuantity();
        String Price = IM.getPrice();
        File ruta = new File(IP.txtruta.getText());

        this.agregar(Code, Brand, Color, Quantity, Price, ruta);
        this.t.visualizar_ProductoVO(IP.InventoryTable);
    }

    public void Update() {
        IM = IP.getInventoryData();
        String Id = IM.getId();
        String Code = IM.getCode();
        String Brand = IM.getBrand();
        String Color = IM.getColor();
        String Quantity = IM.getQuantity();
        String Price = IM.getPrice();
        File ruta = new File(IP.txtruta.getText());
        
        if(ruta.length()!=0){
            this.modify(Id, Code, Brand, Color, Quantity, Price, ruta);
            this.t.visualizar_ProductoVO(IP.InventoryTable);
        }else{
            this.modify2(Id, Code, Brand, Color, Quantity, Price); 
            this.t.visualizar_ProductoVO(IP.InventoryTable);
        }
    }

    public void Delete() {
        IM = IP.getInventoryData();
        String Code = IM.getId();
        this.delete(Code);
        this.t.visualizar_ProductoVO(IP.InventoryTable);
    }

    public void Image() {
        IP.icono();
    }

    public void agregar(String Code, String Brand, String Color, String Quantity, String Price, File ruta) {
        InventoryModel vo = new InventoryModel();
        dao = new InventoryProcess();

        vo.setCode(Code);
        vo.setBrand(Brand);
        vo.setColor(Color);
        vo.setQuantity(Quantity);
        vo.setPrice(Price);
        try {
            byte[] icono = new byte[(int) ruta.length()];
            InputStream input = new FileInputStream(ruta);
            input.read(icono);
            vo.setPreview(icono);
        } catch (Exception ex) {
            vo.setPreview(null);
        }
        dao.Agregar_ProductoVO(vo);
        //limpiar();
    }

    public void modify(String Id, String Code, String Brand, String Color, String Quantity, String Price, File ruta) {
        InventoryModel vo = new InventoryModel();
        dao = new InventoryProcess();

        vo.setId(Id);
        vo.setCode(Code);
        vo.setBrand(Brand);
        vo.setColor(Color);
        vo.setQuantity(Quantity);
        vo.setPrice(Price);
        try {
            byte[] icono = new byte[(int) ruta.length()];
            InputStream input = new FileInputStream(ruta);
            input.read(icono);
            vo.setPreview(icono);
            
        } catch (Exception ex) {
            vo.setPreview(null);
        }
        dao.Modificar_ProductoVO(vo);
        //limpiar();
    }
    
        public void modify2(String Id, String Code, String Brand, String Color, String Quantity, String Price) {
        InventoryModel vo = new InventoryModel();
        dao = new InventoryProcess();

        vo.setId(Id);
        vo.setCode(Code);
        vo.setBrand(Brand);
        vo.setColor(Color);
        vo.setQuantity(Quantity);
        vo.setPrice(Price);

        dao.Modificar_ProductoVO2(vo);
        //limpiar();
        }

    public void delete(String Code) {
        InventoryModel vo = new InventoryModel();
        dao = new InventoryProcess();

        vo.setId(Code);

        dao.Eliminar_ProductoVO(vo);
        //limpiar();
    }
}
