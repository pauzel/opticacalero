package Controller;

import ConnectionSql.ConnectionSQLServer;
import Misc.Result;
import Model.LoginModel;
import View.LoginForm;
import View.ProgressBar;
import java.awt.event.*;
import java.sql.*;
import javax.swing.JOptionPane;

public class LoginController implements ActionListener {

    ConnectionSQLServer cc = new ConnectionSQLServer();
    Connection con = cc.getConnection();

    LoginForm LF;
    LoginModel LM;
    ProgressBar PB = new ProgressBar();

    public LoginController(LoginForm LF) {
        super();
        this.LF = LF;
        LM = new LoginModel();
        LF.error.setVisible(false);
    }

    public LoginModel getLM() {
        return LM;
    }

    public void setLM(LoginModel LM) {
        this.LM = LM;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Login":
                login();
                break;
        }
    }

    public void login() {
        LM = LF.GetData();
        Result r = new Result();
        try {
            String User = LM.getUser();
            String Password = LM.getPassword();

            String sql = " DECLARE @n TINYINT,@nm VARCHAR(500)"
                    + " EXEC LoginProgram ?,?,@n OUTPUT,@nm OUTPUT "
                    + " SELECT @n Codigo,@nm Mensaje";

            ResultSet rs = null;
            PreparedStatement ps = null;

            ps = cc.getConnection().prepareStatement(sql);

            ps.setString(1, User);
            ps.setString(2, Password);

            rs = ps.executeQuery();

            if (User.isEmpty()) {
                LF.error.setVisible(true);
                LF.error.setText("User is empty");
                LF.Clean();
            } else if (Password.isEmpty()) {
                LF.error.setVisible(true);
                LF.error.setText("Password is empty");
                LF.Clean();
            } else if (rs.next()) {
                String Rotulo = rs.getString("Codigo");
                if (Rotulo.equals("1")) {
                    LF.error.setVisible(true);
                    LF.error.setText(rs.getString("Mensaje"));
                    LF.Clean();
                } else if (Rotulo.equals("0")) {
                    
                    String sql1 = "EXEC LoginFull ?";
                    ResultSet rs1 = null;
                    PreparedStatement ps1 = null;

                    ps1 = cc.getConnection().prepareStatement(sql1);

                    ps1.setString(1, User);

                    rs1 = ps1.executeQuery();
                    
                    if (rs1.next()) {
                        String Role = rs1.getString("Role");
                        if (Role.equals("Admin")) {
                            LF.dispose();
                            PB.setVisible(true);
                            PB.tiempoAdmin.start();
                        } else if (Role.equals("Employee")) {
                            LF.dispose();
                            PB.setVisible(true);
                            PB.tiempoEmployee.start();
                        }
                    }
                }
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
