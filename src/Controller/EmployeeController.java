/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import View.EmployeeForm;
import View.LoginForm;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import paneles.CambiaPanel;

/**
 *
 * @author AlexPc
 */
public class EmployeeController implements ActionListener{
    EmployeeForm employee;
    
    public EmployeeController(EmployeeForm employee){
        super();
        this.employee = employee;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "HomeButton":
                Home();
                break;
            case "sales":
                Sales();
                break;
            case "optometry":
                Optometry();
                break;
            case "registerclient":
                registerclient();
                break;
            case "LogOutButton":
                LogOut();
                break;
        }
    }
    
    private void Home(){
        new CambiaPanel(employee.principaljPanel, new paneles.homePanel());
    }
    private void Sales(){
        new CambiaPanel(employee.principaljPanel, new paneles.Sales());
    }
    private void Optometry(){
        new CambiaPanel(employee.principaljPanel, new paneles.Optometry());
    }
    private void registerclient(){
        new CambiaPanel(employee.principaljPanel, new paneles.clientPanel());
    }
    private void LogOut(){
        int confirmacion = JOptionPane.showConfirmDialog(null,  "¿Desea Cerrar Sesion?","" ,JOptionPane.YES_NO_OPTION);
       
       switch (confirmacion){
           case 0:{
               
               LoginForm l = new LoginForm();
               l.setVisible(true);
               employee.dispose();
               break;
           }
       }
    }
}
