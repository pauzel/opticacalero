package Controller;

import ConnectionSql.ConnectionSQLServer;
import Misc.ShowTables;
import Model.SalesModel;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Iterator;
import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import paneles.Sales;

public class SalesController implements ActionListener {

    Sales s;
    SalesModel sm;
    ShowTables STB = new ShowTables();
    ConnectionSQLServer cc = new ConnectionSQLServer();
    Connection con = cc.getConnection();

    public SalesController(Sales s) {
        super();
        this.s = s;
        sm = new SalesModel();
        STB.LoadClientTable(s.salesTable);
    }

    public SalesModel getSm() {
        return sm;
    }

    public void setSm(SalesModel sm) {
        this.sm = sm;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "SearchID":
                searchid();
                break;
            case "SearchCode":
                searchcode();
                break;
            case "GenerateSales":
                generatesales();
                break;
        }
    }

    public void searchid() {
        sm = s.getSalesData();

        String ID_Client = sm.getID_Client();

        try {
            String sql = " EXEC ClientSearchIDOptometry ?";
            ResultSet rs = null;
            PreparedStatement ps = null;

            ps = cc.getConnection().prepareStatement(sql);

            ps.setString(1, ID_Client);

            rs = ps.executeQuery();

            if (rs.next()) {
                s.examdateTextField.setText(rs.getString("Exam_Date"));
                s.medicationTextField.setText(rs.getString("Medication"));
                s.observationTextArea.setText(rs.getString("Observation"));
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void searchcode() {
        sm = s.getSalesData();

        String Code = sm.getCode_Prod();

        try {
            String sql = " EXEC InventorySearchSales ?";
            ResultSet rs = null;
            PreparedStatement ps = null;

            ps = cc.getConnection().prepareStatement(sql);

            ps.setString(1, Code);

            rs = ps.executeQuery();

            if (rs.next()) {
                s.brandTextField.setText(rs.getString("Brand"));
                s.colorTextField.setText(rs.getString("Color"));
                byte[] img = rs.getBytes("Preview");
                Image imagen = getImage(img, false);
                Icon icon = new ImageIcon(imagen);
                s.previewLabel.setIcon(icon);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void generatesales() {
        sm = s.getSalesData();

        String ID_Client = sm.getID_Client();
        String Exam_Date = sm.getDate_Exam();
        String Medication = sm.getMedication();
        String Observation = sm.getObservation();
        String Code = sm.getCode_Prod();
        String Brand = sm.getBrand();
        String Color = sm.getColor();

        try {
            String sql = "DECLARE @n TINYINT,@nm VARCHAR(500);"
                    + " EXEC SalesInsert ?,?,?,?,?,?,?,@n OUTPUT,@nm OUTPUT;"
                    + "Select @n Codigo,@nm Mensaje";
            
            ResultSet rs = null;
            PreparedStatement ps = null;

            ps = cc.getConnection().prepareStatement(sql);

            ps.setString(1, ID_Client);
            ps.setString(2, Exam_Date);
            ps.setString(3, Medication);
            ps.setString(4, Observation);
            ps.setString(5, Code);
            ps.setString(6, Brand);
            ps.setString(7, Color);

            rs = ps.executeQuery();

            if (rs.next()) {
                String bandera = rs.getString("Codigo");

                if (bandera.equals("0")) {
                    s.errorLabel.setText(rs.getString("Mensaje"));
                    STB.LoadSalesTable(s.salesTable);
                } else if (bandera.equals("1")) {
                    s.errorLabel.setText(rs.getString("Mensaje"));
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private Image getImage(byte[] bytes, boolean isThumbnail) throws IOException {

        ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
        Iterator readers = ImageIO.getImageReadersByFormatName("png");
        ImageReader reader = (ImageReader) readers.next();
        Object source = bis; // File or InputStream
        ImageInputStream iis = ImageIO.createImageInputStream(source);
        reader.setInput(iis, true);
        ImageReadParam param = reader.getDefaultReadParam();
        if (isThumbnail) {

            param.setSourceSubsampling(4, 4, 0, 0);

        }
        return reader.read(0, param);

    }

}
