package Controller;

import ConnectionSql.ConnectionSQLServer;
import Model.SalesRecordModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import static javax.swing.text.html.HTML.Tag.MAP;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import paneles.salesPanel;

public class SalesRecordController implements ActionListener {
    
    private final salesPanel s;
    SalesRecordModel sm;
    ConnectionSQLServer cc = new ConnectionSQLServer();
    Connection con = cc.getConnection();
    
    public SalesRecordController(salesPanel s) {
        super();
        this.s = s;
    }
    
    public SalesRecordModel getSm() {
        return sm;
    }
    
    public void setSm(SalesRecordModel sm) {
        this.sm = sm;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "salesreport":
                reportsalesrecord();
                break;
        }
    }
    
    public void reportsalesrecord() {
        sm = s.setData();
        String ID_Sales = sm.getID_Sales();
        
        if(!ID_Sales.isEmpty()){
            try {
                String rutareporte = System.getProperty("user.dir") + "/src/Reports/Reportes_Ventas_Totales.jasper";
                String Logo = System.getProperty("user.dir") + "/src/resources/logoventana.png";
                String Icono = System.getProperty("user.dir") + "/src/resources/icono.png";
                Map<String,Object> parameters = new HashMap<>();
                parameters.put("Logo",Logo);
                parameters.put("Icono",Icono);
                parameters.put("ID_Sales", ID_Sales);
                JasperReport jasperreport = (JasperReport) JRLoader.loadObjectFromFile(rutareporte); 
                JasperPrint print = null;
                print = JasperFillManager.fillReport(jasperreport,parameters, con);              
                JasperViewer view = new JasperViewer(print, false);
                view.setIconImage(new ImageIcon(getClass().getResource("/resources/logoventana.png")).getImage());
                view.setTitle("Sales Record For ID_Sales");
                view.setVisible(true);          
            } catch (Exception e) {System.out.println(e.getMessage());}
        }else{
            try {
                String rutareporte = System.getProperty("user.dir") + "/src/Reports/Reportes_Ventas_Totales.jasper";
                String Logo = System.getProperty("user.dir") + "/src/resources/logoventana.png";
                String Icono = System.getProperty("user.dir") + "/src/resources/icono.png";
                Map<String,Object> parameters = new HashMap<>();
                parameters.put("Logo",Logo);
                parameters.put("Icono",Icono);
                JasperReport jasperreport = (JasperReport) JRLoader.loadObjectFromFile(rutareporte); 
                JasperPrint print = null;
                print = JasperFillManager.fillReport(jasperreport, parameters, con);                
                JasperViewer view = new JasperViewer(print, false);
                view.setIconImage(new ImageIcon(getClass().getResource("/resources/logoventana.png")).getImage());
                view.setTitle("Sales Record For ID_Sales");
                view.setVisible(true);          
            } catch (Exception e) {System.out.println(e.getMessage());}            
        }
      
    }    
}
