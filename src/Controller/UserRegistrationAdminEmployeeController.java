package Controller;

import ConnectionSql.ConnectionSQLServer;
import Misc.ShowTables;
import Model.UserRegistrationAdminEmployeeModel;
import java.awt.Panel;
import java.awt.event.*;
import java.io.*;
import java.net.*;
import java.sql.*;
import java.util.Properties;
import java.util.logging.*;
import java.util.regex.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import paneles.registrationPanel;

public class UserRegistrationAdminEmployeeController implements ActionListener {

    ConnectionSQLServer cc = new ConnectionSQLServer();
    Connection con = cc.getConnection();

    registrationPanel RP;
    UserRegistrationAdminEmployeeModel URAEM;
    ShowTables STB = new ShowTables();

    public UserRegistrationAdminEmployeeController(registrationPanel RP) {
        super();
        this.RP = RP;
        URAEM = new UserRegistrationAdminEmployeeModel();
        RP.errorLabel.setVisible(false);
        STB.LoadUserTable(RP.UserTable);
    }

    public UserRegistrationAdminEmployeeModel getURAEM() {
        return URAEM;
    }

    public void setURAEM(UserRegistrationAdminEmployeeModel URAEM) {
        this.URAEM = URAEM;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {

            case "InsertButton":
                Insert();
                break;

            case "ModifyButton":
                Modify();
                break;

            case "DeleteButton":
                Delete();
                break;

            case "CleanButton":
                Clean();
                break;
        }
    }

    private void Insert() {

        URAEM = RP.getUserData();

        try {
            String ID_User = URAEM.getID_User();
            String ID_Ced = URAEM.getID_Ced();
            String User = URAEM.getUser();
            String Pass = URAEM.getPassword();
            String Name = URAEM.getName();
            String LastName = URAEM.getLastName();
            String Phone = URAEM.getPhone();
            String Mail = URAEM.getMail();
            String Department = URAEM.getDepartment();
            String Municipality = URAEM.getMunicipality();
            String Role = URAEM.getRole();
            String Direction = URAEM.getDirection();
            String RecoveryCode = URAEM.getRecvoeryCode();

            Pattern pattern = Pattern.compile("([a-z0-9]+(\\.?[a-z0-9])*)+@(([a-z]+)\\.([a-z]+))+");
            String email = Mail;
            Matcher mather = pattern.matcher(email);

            if (ID_Ced.length() == 14) {
                if (!Character.isLetter(ID_Ced.charAt(13))) {
                    RP.errorLabel.setVisible(true);
                    RP.errorLabel.setText("Formato de cedula incorrecto");
                } else {
                    if (mather.find() == true) {

                        String sql = "DECLARE  @n TINYINT, @nm VARCHAR(500);"
                                + "EXEC UserInsert ?,?,?,?,?,?,?,?,?,?,?,@n OUTPUT,@nm OUTPUT;"
                                + "select @n Codigo,@nm Mensaje";

                        ResultSet rs = null;
                        PreparedStatement ps = null;

                        ps = cc.getConnection().prepareStatement(sql);

                        ps.setString(1, ID_Ced);
                        ps.setString(2, User);
                        ps.setString(3, Pass);
                        ps.setString(4, Name);
                        ps.setString(5, LastName);
                        ps.setString(6, Phone);
                        ps.setString(7, Mail);
                        ps.setString(8, Department);
                        ps.setString(9, Municipality);
                        ps.setString(10, Role);
                        ps.setString(11, Direction);

                        rs = ps.executeQuery();

                        if (rs.next()) {
                            STB.LoadUserTable(RP.UserTable);
                            tiempo.start();
                            Enviar();
                            SendWhatsapp();
                            sendSMS();
                        }
                    } else {
                        RP.errorLabel.setVisible(true);
                        RP.errorLabel.setText("El Mail ingresado debe ser valido");
                    }

                }
            } else {
                RP.errorLabel.setVisible(true);
                RP.errorLabel.setText("Formato de cedula incorrecto");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void Modify() {
        URAEM = RP.getUserData();

        String ID_User = URAEM.getID_User();
        String ID_Ced = URAEM.getID_Ced();
        String User = URAEM.getUser();
        String Name = URAEM.getName();
        String LastName = URAEM.getLastName();
        String Phone = URAEM.getPhone();
        String Mail = URAEM.getMail();
        String Department = URAEM.getDepartment();
        String Municipality = URAEM.getMunicipality();
        String Role = URAEM.getRole();
        String Direction = URAEM.getDirection();
        String RecoveryCode = URAEM.getRecvoeryCode();

        if (ID_Ced.length() == 14) {
            if (!Character.isLetter(ID_Ced.charAt(13))) {
                RP.errorLabel.setVisible(true);
                RP.errorLabel.setText("Formato de cedula incorrecto");
            } else {
                try {

                    String sql = "DECLARE @n int,@nm varchar(50);"
                            + "EXEC UserUpdate ?,?,?,?,?,?,?,?,?,?,?,@n output,@nm output;"
                            + "SELECT @n Codigo,@nm Mensaje";

                    ResultSet rs = null;
                    PreparedStatement ps = null;

                    ps = cc.getConnection().prepareStatement(sql);

                    ps.setString(1, ID_User);
                    ps.setString(2, ID_Ced);
                    ps.setString(3, User);
                    ps.setString(4, Name);
                    ps.setString(5, LastName);
                    ps.setString(6, Phone);
                    ps.setString(7, Mail);
                    ps.setString(8, Department);
                    ps.setString(9, Municipality);
                    ps.setString(10, Role);
                    ps.setString(11, Direction);

                    rs = ps.executeQuery();

                    if (rs.next()) {
                        String valida = rs.getString("Codigo");

                        if (valida.equals("0")) {
                            STB.LoadUserTable(RP.UserTable);
                            RP.errorLabel.setVisible(true);
                            RP.errorLabel.setText(rs.getString("Mensaje"));

                        } else if (valida.equals("1")) {
                            RP.errorLabel.setVisible(true);
                            RP.errorLabel.setText(rs.getString("Mensaje"));
                        }

                    }

                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
        } else {
            RP.errorLabel.setVisible(true);
            RP.errorLabel.setText("Formato de cedula incorrecto");
        }
    }

    private void Delete() {
        try {
            URAEM = RP.getUserData();
            String ID_Ced = URAEM.getID_Ced();
            String sql = "DECLARE  @n TINYINT, @nm VARCHAR(500);"
                    + "EXEC UserDelete ?,@n OUTPUT,@nm OUTPUT;"
                    + "select @n Codigo,@nm Mensaje";

            ResultSet rs = null;
            PreparedStatement ps = null;

            ps = cc.getConnection().prepareStatement(sql);

            ps.setString(1, ID_Ced);

            rs = ps.executeQuery();

            if (rs.next()) {
                STB.LoadUserTable(RP.UserTable);
                RP.errorLabel.setVisible(true);
                RP.errorLabel.setText(rs.getString("Mensaje"));
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void Clean() {
        RP.Clean();
    }

    Thread tiempo = new Thread() {
        public void run() {
            try {
                for (int i = 1; i <= 100; i++) {
                    RP.errorLabel.setText("Entered Correctly");
                    tiempo.sleep(10);
                    if (i == 100) {
                        RP.errorLabel.setVisible(false);
                    }
                }
            } catch (Exception e) {
            }
        }
    };

    public void Enviar() {
        try {
            URAEM = RP.getUserData();
            String user = URAEM.getUser();
            String SQL1 = "SELECT * FROM dbo.Users WHERE [User] = ?";

            ResultSet rs1 = null;
            PreparedStatement ps = null;

            ps = cc.getConnection().prepareStatement(SQL1);

            ps.setString(1, user);

            rs1 = ps.executeQuery();

            while (rs1.next()) {
                Properties propiedad = new Properties();
                propiedad.setProperty("mail.smtp.host", "smtp.gmail.com");
                propiedad.setProperty("mail.smtp.starttls.enable", "true");
                propiedad.setProperty("mail.smtp.port", "587");
                propiedad.setProperty("mail.smtp.auth", "true");

                Session sesion = Session.getDefaultInstance(propiedad);
                String correoEnvia = "opticascalero@gmail.com";
                String contraseña = "123Putito123";
                String destinatario = URAEM.getMail();
                String Asunto = "Succefully Registration";

                MimeMessage mail = new MimeMessage(sesion);
                try {
                    mail.setFrom(new InternetAddress(correoEnvia));
                    mail.addRecipient(Message.RecipientType.TO, new InternetAddress(destinatario));
                    mail.setSubject(Asunto);
                    String Usuario = rs1.getString("User");
                    String Recovery = rs1.getString("RecoveryCode");
                    mail.setText("Successful registration in optica calero, welcome!\n----------------------------------"
                            + "\n\nDear " + Usuario + "\n\nWe hope you like your stay with this family");

                    Transport transportar = sesion.getTransport("smtp");
                    transportar.connect(correoEnvia, contraseña);
                    transportar.sendMessage(mail, mail.getRecipients(Message.RecipientType.TO));
                    transportar.close();

                } catch (AddressException ex) {
                    Logger.getLogger(Panel.class.getName()).log(Level.SEVERE, null, ex);
                } catch (MessagingException ex) {
                    Logger.getLogger(Panel.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            rs1.close();
            //stmt.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void SendWhatsapp() {
        URAEM = RP.getUserData();
        try {
            String SQL1 = "SELECT * FROM dbo.Users WHERE [User] = ?";

            ResultSet rs1 = null;
            PreparedStatement ps = null;

            ps = cc.getConnection().prepareStatement(SQL1);

            ps.setString(1, URAEM.getUser());

            rs1 = ps.executeQuery();

            if (rs1.next()) {
                String Usuario = rs1.getString("User");
                String Recovery = rs1.getString("RecoveryCode");
                String yourId = "zPAYHrrL9EeQFP2Pfdf4ymFwYXVsaW5vam96enp6X2F0X2dtYWlsX2RvdF9jb20=";
                String yourMobile = URAEM.getPhone();
                String yourMessage = "Successful registration in optica calero, welcome!\n----------------------------------"
                        + "\n\nDear " + Usuario + "\n\nWe hope you like your stay with this family";
                HttpURLConnection connection = null;
                try {
                    URL url = new URL("https://NiceApi.net/API");
                    connection = (HttpURLConnection) url.openConnection();
                    connection.setRequestMethod("POST");
                    connection.setRequestProperty("X-APIId", yourId);
                    connection.setRequestProperty("X-APIMobile", yourMobile);
                    connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    connection.setUseCaches(false);
                    connection.setDoOutput(true);
                    DataOutputStream streamOut = new DataOutputStream(connection.getOutputStream());
                    streamOut.writeBytes(yourMessage);
                    streamOut.close();
                    InputStream streamIn = connection.getInputStream();
                    BufferedReader readerIn = new BufferedReader(new InputStreamReader(streamIn));
                    System.out.println(readerIn.readLine());
                    readerIn.close();
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                } finally {
                    if (connection != null) {
                        connection.disconnect();
                    }
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public void sendSMS() throws SQLException {
        URAEM = RP.getUserData();
        try {
            String SQL1 = "SELECT * FROM dbo.Users WHERE [User] = ?";
            ResultSet rs1 = null;
            PreparedStatement ps = null;

            ps = cc.getConnection().prepareStatement(SQL1);

            ps.setString(1, URAEM.getUser());

            rs1 = ps.executeQuery();

            if (rs1.next()) {
                String Usuario = rs1.getString("User");
                String data = "";
                data += "username=" + URLEncoder.encode("23zelaya23", "ISO-8859-1");
                data += "&password=" + URLEncoder.encode("Gisel1234@", "ISO-8859-1");
                data += "&message=" + URLEncoder.encode("Successful registration in optica calero, welcome!\n----------------------------------"
                        + "\n\nDear " + Usuario + "\n\nWe hope you like your stay with this family", "ISO-8859-1");
                data += "&want_report=1";
                data += "&msisdn=+" + URAEM.getPhone() + "";

                URL url = new URL("https://bulksms.vsms.net/eapi/submission/send_sms/2/2.0");

                URLConnection conn = url.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(data);
                wr.flush();

                BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String line;
                while ((line = rd.readLine()) != null) {
                    System.out.println(line);
                }
                wr.close();
                rd.close();
            }
        } catch (IOException e) {

        }
    }
}
