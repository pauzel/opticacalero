package paneles;

import ConnectionSql.ConnectionSQLServer;
import Controller.ClientController;
import Misc.DesignTable;
import Misc.ShowTables;
import Model.ClientModel;
import java.awt.Color;
import java.awt.Cursor;
import static java.awt.Frame.HAND_CURSOR;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Connection;
import java.util.Vector;
import javax.swing.Icon;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

public class clientPanel extends javax.swing.JPanel {

    ShowTables STB = new ShowTables();
    ConnectionSQLServer cc = new ConnectionSQLServer();
    Connection con = cc.getConnection();
    
    private ClientController CC;
    
    public clientPanel() {
        initComponents();
        performance();
    }
    
    private void performance(){
        STB.LoadClientTable(ClientTable);
        setController();
        idTextField.setBackground(new java.awt.Color(0,0,0,1));
        cedFormattedTextField.setBackground(new java.awt.Color(0,0,0,1));
        nameTextField.setBackground(new java.awt.Color(0,0,0,1));
        lastnameTextField.setBackground(new java.awt.Color(0,0,0,1));
        ageTextField.setBackground(new java.awt.Color(0,0,0,1));
        directionTextField.setBackground(new java.awt.Color(0,0,0,1));
        mailTextField.setBackground(new java.awt.Color(0,0,0,1));
        phoneFormattedTextField.setBackground(new java.awt.Color(0,0,0,1));
        searchclientTextField.setBackground(new java.awt.Color(0,0,0,1));
        jSeparator1.setBackground(new java.awt.Color(0,0,0,10));
        this.addclientButton.setCursor(new Cursor(HAND_CURSOR));
        this.modifyClientButton.setCursor(new Cursor(HAND_CURSOR));
        this.deleteClientButton.setCursor(new Cursor(HAND_CURSOR));
        this.CleanButton.setCursor(new Cursor(HAND_CURSOR));
        this.searchclientButton.setCursor(new Cursor(HAND_CURSOR));
    }
    
    private void setController(){
        CC = new ClientController(this);
        addclientButton.addActionListener(CC);
        modifyClientButton.addActionListener(CC);
        deleteClientButton.addActionListener(CC);
        CleanButton.addActionListener(CC);
    }
    
    public void clientData(ClientModel CM){
        idTextField.setText(CM.getID());
        cedFormattedTextField.setText(CM.getCed());
        nameTextField.setText(CM.getName());
        lastnameTextField.setText(CM.getLastName());
        ageTextField.setText(CM.getAge());
        directionTextField.setText(CM.getDirection());
        mailTextField.setText(CM.getMail());
        phoneFormattedTextField.setText(CM.getPhone());
    }
    
    public ClientModel getClientData(){
        ClientModel CM = new ClientModel();
        CM.setID(idTextField.getText());
        CM.setCed(cedFormattedTextField.getText());
        CM.setName(nameTextField.getText());
        CM.setLastName(lastnameTextField.getText());
        CM.setAge(ageTextField.getText());
        CM.setDirection(directionTextField.getText());
        CM.setMail(mailTextField.getText());
        CM.setPhone(phoneFormattedTextField.getText());
        return CM;
    }
    
    public void Clean(){
        idTextField.setText("");
        cedFormattedTextField.setText("");
        nameTextField.setText("");
        lastnameTextField.setText("");
        ageTextField.setText("");
        directionTextField.setText("");
        mailTextField.setText("");
        phoneFormattedTextField.setText("");
    }
 
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSeparator9 = new javax.swing.JSeparator();
        jSeparator8 = new javax.swing.JSeparator();
        jSeparator7 = new javax.swing.JSeparator();
        jSeparator6 = new javax.swing.JSeparator();
        jSeparator5 = new javax.swing.JSeparator();
        jSeparator4 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator1 = new javax.swing.JSeparator();
        directionTextField = new javax.swing.JTextField();
        ageTextField = new javax.swing.JTextField();
        lastnameTextField = new javax.swing.JTextField();
        nameTextField = new javax.swing.JTextField();
        idTextField = new javax.swing.JTextField();
        mailTextField = new javax.swing.JTextField();
        phoneFormattedTextField = new javax.swing.JFormattedTextField();
        searchclientButton = new javax.swing.JButton();
        modifyClientButton = new javax.swing.JButton();
        deleteClientButton = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        addclientButton = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        CleanButton = new javax.swing.JButton();
        ErrorLabel = new javax.swing.JLabel();
        searchclientTextField = new app.bolivia.swing.JCTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        ClientTable = new javax.swing.JTable();
        cedFormattedTextField = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        add(jSeparator9, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 330, 270, 10));
        add(jSeparator8, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 180, 120, 10));
        add(jSeparator7, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 150, 220, 10));
        add(jSeparator6, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 120, 220, 10));
        add(jSeparator5, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 90, 70, 10));
        add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 180, 170, 10));
        add(jSeparator3, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 150, 170, 10));
        add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 120, 170, 10));
        add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 90, 70, 10));

        directionTextField.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        directionTextField.setForeground(new java.awt.Color(255, 255, 255));
        directionTextField.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        directionTextField.setBorder(null);
        add(directionTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 100, 220, 20));

        ageTextField.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        ageTextField.setForeground(new java.awt.Color(255, 255, 255));
        ageTextField.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        ageTextField.setBorder(null);
        ageTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                ageTextFieldKeyTyped(evt);
            }
        });
        add(ageTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 70, 70, 20));

        lastnameTextField.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lastnameTextField.setForeground(new java.awt.Color(255, 255, 255));
        lastnameTextField.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        lastnameTextField.setBorder(null);
        lastnameTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                lastnameTextFieldKeyTyped(evt);
            }
        });
        add(lastnameTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 160, 170, 20));

        nameTextField.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        nameTextField.setForeground(new java.awt.Color(255, 255, 255));
        nameTextField.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        nameTextField.setBorder(null);
        nameTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                nameTextFieldKeyTyped(evt);
            }
        });
        add(nameTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 130, 170, 20));

        idTextField.setEditable(false);
        idTextField.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        idTextField.setForeground(new java.awt.Color(255, 255, 255));
        idTextField.setBorder(null);
        add(idTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 70, 70, 20));

        mailTextField.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        mailTextField.setForeground(new java.awt.Color(255, 255, 255));
        mailTextField.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        mailTextField.setBorder(null);
        add(mailTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 130, 220, 20));

        phoneFormattedTextField.setBorder(null);
        phoneFormattedTextField.setForeground(new java.awt.Color(255, 255, 255));
        try {
            phoneFormattedTextField.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("####-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        phoneFormattedTextField.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        phoneFormattedTextField.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        add(phoneFormattedTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 160, 120, 20));

        searchclientButton.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        searchclientButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/searchclientButton.png"))); // NOI18N
        searchclientButton.setBorder(null);
        searchclientButton.setBorderPainted(false);
        searchclientButton.setContentAreaFilled(false);
        searchclientButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                searchclientButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                searchclientButtonMouseExited(evt);
            }
        });
        add(searchclientButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 300, -1, 30));

        modifyClientButton.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        modifyClientButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/modifyButton.png"))); // NOI18N
        modifyClientButton.setActionCommand("ModifyClient");
        modifyClientButton.setBorder(null);
        modifyClientButton.setBorderPainted(false);
        modifyClientButton.setContentAreaFilled(false);
        modifyClientButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                modifyClientButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                modifyClientButtonMouseExited(evt);
            }
        });
        add(modifyClientButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 210, 110, 30));

        deleteClientButton.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        deleteClientButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/deleteButton.png"))); // NOI18N
        deleteClientButton.setActionCommand("DeleteClient");
        deleteClientButton.setBorder(null);
        deleteClientButton.setBorderPainted(false);
        deleteClientButton.setContentAreaFilled(false);
        deleteClientButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                deleteClientButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                deleteClientButtonMouseExited(evt);
            }
        });
        add(deleteClientButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 210, 110, 30));

        jLabel9.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("Phone:");
        add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 160, -1, 30));

        addclientButton.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        addclientButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/addclientButton.png"))); // NOI18N
        addclientButton.setActionCommand("AddClient");
        addclientButton.setBorder(null);
        addclientButton.setBorderPainted(false);
        addclientButton.setContentAreaFilled(false);
        addclientButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                addclientButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                addclientButtonMouseExited(evt);
            }
        });
        add(addclientButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 210, 110, 30));

        jLabel8.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Mail:");
        add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 130, -1, 30));

        jLabel6.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Direction:");
        add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 100, -1, 30));

        jLabel7.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Ced:");
        add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 100, -1, 30));

        jLabel5.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Age:");
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 70, -1, 30));

        jLabel4.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Last name:");
        add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 160, -1, 30));

        jLabel3.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Name:");
        add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 130, -1, 30));

        jLabel2.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Id:");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 70, -1, 20));

        CleanButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/cleanButton.png"))); // NOI18N
        CleanButton.setActionCommand("Clean");
        CleanButton.setBorder(null);
        CleanButton.setBorderPainted(false);
        CleanButton.setContentAreaFilled(false);
        CleanButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                CleanButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                CleanButtonMouseExited(evt);
            }
        });
        add(CleanButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 210, -1, 30));

        ErrorLabel.setBackground(new java.awt.Color(204, 0, 0));
        ErrorLabel.setForeground(new java.awt.Color(255, 0, 0));
        add(ErrorLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 40, 320, 20));

        searchclientTextField.setBorder(null);
        searchclientTextField.setForeground(new java.awt.Color(255, 255, 255));
        searchclientTextField.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        searchclientTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        searchclientTextField.setOpaque(false);
        searchclientTextField.setPhColor(new java.awt.Color(255, 255, 255));
        searchclientTextField.setPlaceholder("                       Search Client");
        searchclientTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchclientTextFieldActionPerformed(evt);
            }
        });
        searchclientTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                searchclientTextFieldKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                searchclientTextFieldKeyTyped(evt);
            }
        });
        add(searchclientTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 300, 270, 30));

        ClientTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Id", "Ced", "Name", "Last name", "Age", "Direction", "Mail", "Phone"
            }
        ));
        ClientTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ClientTableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(ClientTable);

        add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 340, 900, 170));

        cedFormattedTextField.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        cedFormattedTextField.setForeground(new java.awt.Color(255, 255, 255));
        cedFormattedTextField.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        cedFormattedTextField.setBorder(null);
        cedFormattedTextField.setOpaque(false);
        cedFormattedTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                cedFormattedTextFieldKeyTyped(evt);
            }
        });
        add(cedFormattedTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 100, 170, 20));

        jLabel11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/viñetaClient.png"))); // NOI18N
        add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 960, 610));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/background_clients.png"))); // NOI18N
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 960, 610));
    }// </editor-fold>//GEN-END:initComponents

    private void ClientTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ClientTableMouseClicked
        int Selection = ClientTable.rowAtPoint(evt.getPoint());
        idTextField.setText(String.valueOf(ClientTable.getValueAt(Selection, 0)));
        cedFormattedTextField.setText(String.valueOf(ClientTable.getValueAt(Selection, 1)));
        nameTextField.setText(String.valueOf(ClientTable.getValueAt(Selection, 2)));
        lastnameTextField.setText(String.valueOf(ClientTable.getValueAt(Selection, 3)));
        ageTextField.setText(String.valueOf(ClientTable.getValueAt(Selection, 4)));
        directionTextField.setText(String.valueOf(ClientTable.getValueAt(Selection, 5)));
        mailTextField.setText(String.valueOf(ClientTable.getValueAt(Selection, 6)));
        phoneFormattedTextField.setText(String.valueOf(ClientTable.getValueAt(Selection, 7)));
    }//GEN-LAST:event_ClientTableMouseClicked

    private void addclientButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addclientButtonMouseEntered
        addclientButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/addclientButtonefecto.png")));
    }//GEN-LAST:event_addclientButtonMouseEntered

    private void addclientButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addclientButtonMouseExited
        addclientButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/addclientButton.png")));

    }//GEN-LAST:event_addclientButtonMouseExited

    private void modifyClientButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_modifyClientButtonMouseEntered
        modifyClientButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/modifyButtonefecto.png")));
    }//GEN-LAST:event_modifyClientButtonMouseEntered

    private void modifyClientButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_modifyClientButtonMouseExited
        modifyClientButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/modifyButton.png")));
    }//GEN-LAST:event_modifyClientButtonMouseExited

    private void deleteClientButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_deleteClientButtonMouseEntered
        deleteClientButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/deleteButtonefecto.png")));
    }//GEN-LAST:event_deleteClientButtonMouseEntered

    private void deleteClientButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_deleteClientButtonMouseExited
        deleteClientButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/deleteButton.png")));
    }//GEN-LAST:event_deleteClientButtonMouseExited

    private void CleanButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_CleanButtonMouseEntered
        CleanButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/CleanButtonefecto.png")));
    }//GEN-LAST:event_CleanButtonMouseEntered

    private void CleanButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_CleanButtonMouseExited
        CleanButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/CleanButton.png")));
    }//GEN-LAST:event_CleanButtonMouseExited

    private void searchclientButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_searchclientButtonMouseEntered
        searchclientButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/searchclientButtonefecto.png")));
    }//GEN-LAST:event_searchclientButtonMouseEntered

    private void searchclientButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_searchclientButtonMouseExited
        searchclientButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/searchclientButton.png")));
    }//GEN-LAST:event_searchclientButtonMouseExited

    private void searchclientTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchclientTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_searchclientTextFieldActionPerformed

    private void searchclientTextFieldKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_searchclientTextFieldKeyReleased
        String Search = searchclientTextField.getText();
        STB.ClientSearchTable(ClientTable, Search);
    }//GEN-LAST:event_searchclientTextFieldKeyReleased

    private void searchclientTextFieldKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_searchclientTextFieldKeyTyped
        char letras = evt.getKeyChar();
        if (Character.isLowerCase(letras)) {
            String cad = ("" + letras).toUpperCase();
            letras = cad.charAt(0);
            evt.setKeyChar(letras);
        }
    }//GEN-LAST:event_searchclientTextFieldKeyTyped

    private void cedFormattedTextFieldKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cedFormattedTextFieldKeyTyped
       char c= evt.getKeyChar();
        if(!(Character.isDigit(c)||Character.isLetter(c)||c==KeyEvent.VK_BACK_SPACE||c==KeyEvent.VK_ENTER)){
                evt.consume();
        }
    }//GEN-LAST:event_cedFormattedTextFieldKeyTyped

    private void ageTextFieldKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ageTextFieldKeyTyped
        int i = evt.getKeyChar();
        if ((i < '0' || i > '9')) {
            Toolkit.getDefaultToolkit().beep();
            evt.consume();
        }
    }//GEN-LAST:event_ageTextFieldKeyTyped

    private void nameTextFieldKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_nameTextFieldKeyTyped
        char validar = evt.getKeyChar();

        if (Character.isDigit(validar)) {
            getToolkit().beep();
            evt.consume();
        }
    }//GEN-LAST:event_nameTextFieldKeyTyped

    private void lastnameTextFieldKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_lastnameTextFieldKeyTyped
        char validar = evt.getKeyChar();

        if (Character.isDigit(validar)) {
            getToolkit().beep();
            evt.consume();
        }
    }//GEN-LAST:event_lastnameTextFieldKeyTyped


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton CleanButton;
    public javax.swing.JTable ClientTable;
    public javax.swing.JLabel ErrorLabel;
    private javax.swing.JButton addclientButton;
    private javax.swing.JTextField ageTextField;
    private javax.swing.JTextField cedFormattedTextField;
    private javax.swing.JButton deleteClientButton;
    private javax.swing.JTextField directionTextField;
    private javax.swing.JTextField idTextField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JSeparator jSeparator9;
    private javax.swing.JTextField lastnameTextField;
    private javax.swing.JTextField mailTextField;
    private javax.swing.JButton modifyClientButton;
    private javax.swing.JTextField nameTextField;
    private javax.swing.JFormattedTextField phoneFormattedTextField;
    private javax.swing.JButton searchclientButton;
    public static app.bolivia.swing.JCTextField searchclientTextField;
    // End of variables declaration//GEN-END:variables
}
