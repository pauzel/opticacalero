package paneles;

import Controller.ChangePasswordController;
import Controller.RecoveryPasswordController;
import Model.ChangePasswordModel;
import Model.RecoveryPasswordModel;
import java.awt.Cursor;
import static java.awt.Frame.HAND_CURSOR;
import java.awt.Toolkit;
import javax.swing.Icon;
import javax.swing.ImageIcon;

public class RecoveryPasswordChange extends javax.swing.JFrame {

    public RecoveryPasswordChange() {
        initComponents();
        setController();
        this.setResizable(false); //EVITAMOS QUE EL JFRAME SE MAXIMIZE
        this.setLocationRelativeTo(this);
        this.changeButton.setCursor(new Cursor(HAND_CURSOR));
        this.exitButton.setCursor(new Cursor(HAND_CURSOR));
    }
    
    public void setController(){
        RecoveryPasswordController rpc = new RecoveryPasswordController(this);
        
        changeButton.addActionListener(rpc);
        exitButton.addActionListener(rpc);
    }
    
    public RecoveryPasswordModel getData(){
        RecoveryPasswordModel rpm = new RecoveryPasswordModel();
        
        rpm.setRecoverycode(recoverycodeTextField.getText());
        rpm.setNewpassword(String.valueOf(newpasswordTextField.getPassword()));
        rpm.setRepeatpassword(String.valueOf(repeatpasswordTextField.getPassword()));
        
        return rpm;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        changeButton = new javax.swing.JButton();
        exitButton = new javax.swing.JButton();
        errorLabel = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        recoverycodeTextField = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        newpasswordTextField = new javax.swing.JPasswordField();
        repeatpasswordTextField = new javax.swing.JPasswordField();
        jSeparator3 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        changeButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/newpasswordButton.png"))); // NOI18N
        changeButton.setActionCommand("New");
        changeButton.setBorderPainted(false);
        changeButton.setContentAreaFilled(false);
        changeButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                changeButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                changeButtonMouseExited(evt);
            }
        });
        getContentPane().add(changeButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 180, 110, 30));

        exitButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/exit.png"))); // NOI18N
        exitButton.setActionCommand("exitchange");
        exitButton.setBorderPainted(false);
        exitButton.setContentAreaFilled(false);
        getContentPane().add(exitButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 0, 30, 30));

        errorLabel.setBackground(new java.awt.Color(255, 0, 51));
        errorLabel.setForeground(new java.awt.Color(204, 0, 0));
        errorLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        getContentPane().add(errorLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 40, 240, 20));

        jLabel2.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel2.setText("An email with the recovery code has been send");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 10, 240, 20));

        jLabel5.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Code:");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, -1, 30));

        recoverycodeTextField.setBackground(new java.awt.Color(39, 7, 82));
        recoverycodeTextField.setFont(new java.awt.Font("Calibri", 1, 11)); // NOI18N
        recoverycodeTextField.setForeground(new java.awt.Color(255, 255, 255));
        recoverycodeTextField.setBorder(null);
        recoverycodeTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                recoverycodeTextFieldKeyTyped(evt);
            }
        });
        getContentPane().add(recoverycodeTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 60, 220, 20));

        jLabel6.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("New Password");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 100, -1, 30));

        jLabel3.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Repeat Pass");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 140, -1, 30));

        newpasswordTextField.setBackground(new java.awt.Color(39, 7, 82));
        newpasswordTextField.setFont(new java.awt.Font("Calibri", 1, 11)); // NOI18N
        newpasswordTextField.setForeground(new java.awt.Color(255, 255, 255));
        newpasswordTextField.setBorder(null);
        getContentPane().add(newpasswordTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 100, 220, 20));

        repeatpasswordTextField.setBackground(new java.awt.Color(39, 7, 82));
        repeatpasswordTextField.setFont(new java.awt.Font("Calibri", 1, 11)); // NOI18N
        repeatpasswordTextField.setForeground(new java.awt.Color(255, 255, 255));
        repeatpasswordTextField.setBorder(null);
        getContentPane().add(repeatpasswordTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 140, 220, 20));
        getContentPane().add(jSeparator3, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 160, 220, 10));
        getContentPane().add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 120, 220, 10));
        getContentPane().add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 80, 220, 10));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/background_recoverypassword.png"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 340, 240));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void changeButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_changeButtonMouseEntered
        changeButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/newpasswordButtonefecto.png")));
    }//GEN-LAST:event_changeButtonMouseEntered

    private void changeButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_changeButtonMouseExited
       changeButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/newpasswordButton.png")));
    }//GEN-LAST:event_changeButtonMouseExited

    private void recoverycodeTextFieldKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_recoverycodeTextFieldKeyTyped
        int i = evt.getKeyChar();
        if ((i < '0' || i > '9')) {
            Toolkit.getDefaultToolkit().beep();
            evt.consume();
        }
    }//GEN-LAST:event_recoverycodeTextFieldKeyTyped

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(RecoveryPasswordChange.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(RecoveryPasswordChange.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(RecoveryPasswordChange.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RecoveryPasswordChange.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new RecoveryPasswordChange().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton changeButton;
    public javax.swing.JLabel errorLabel;
    private javax.swing.JButton exitButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JPasswordField newpasswordTextField;
    private javax.swing.JTextField recoverycodeTextField;
    private javax.swing.JPasswordField repeatpasswordTextField;
    // End of variables declaration//GEN-END:variables
}
