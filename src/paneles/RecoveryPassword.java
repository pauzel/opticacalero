package paneles;

import Controller.ChangePasswordController;
import Controller.RecoveryPasswordController;
import Model.ChangePasswordModel;
import Model.RecoveryPasswordModel;
import java.awt.Cursor;
import static java.awt.Frame.HAND_CURSOR;
import javax.swing.Icon;
import javax.swing.ImageIcon;

public class RecoveryPassword extends javax.swing.JFrame {

    public RecoveryPassword() {
        initComponents();
        setController();
        this.setResizable(false); //EVITAMOS QUE EL JFRAME SE MAXIMIZE
        this.setLocationRelativeTo(this);
        this.changeButton.setCursor(new Cursor(HAND_CURSOR));
        this.exitButton.setCursor(new Cursor(HAND_CURSOR));
    }
    
    public void setController(){
        RecoveryPasswordController rpc = new RecoveryPasswordController(this);
        
        changeButton.addActionListener(rpc);
        exitButton.addActionListener(rpc);
    }
    
    public RecoveryPasswordModel getData(){
        RecoveryPasswordModel rpm = new RecoveryPasswordModel();
        
        rpm.setMail(mailTextField.getText());
        
        return rpm;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSeparator1 = new javax.swing.JSeparator();
        changeButton = new javax.swing.JButton();
        exitButton = new javax.swing.JButton();
        errorLabel = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        mailTextField = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        changeButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/sendButton.png"))); // NOI18N
        changeButton.setActionCommand("Change");
        changeButton.setBorderPainted(false);
        changeButton.setContentAreaFilled(false);
        changeButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                changeButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                changeButtonMouseExited(evt);
            }
        });
        getContentPane().add(changeButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 170, 110, 30));

        exitButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/exit.png"))); // NOI18N
        exitButton.setActionCommand("exit");
        exitButton.setBorderPainted(false);
        exitButton.setContentAreaFilled(false);
        getContentPane().add(exitButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 0, 30, 30));

        errorLabel.setBackground(new java.awt.Color(255, 0, 51));
        errorLabel.setForeground(new java.awt.Color(204, 0, 0));
        errorLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        getContentPane().add(errorLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 110, 240, 20));

        jLabel2.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel2.setText("Dear user, in order to verify your account authenticity,");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 30, 270, 20));

        jLabel3.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText(" please enter the email provided to send you a password ");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 50, 290, -1));

        jLabel4.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("recovery code.");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 70, -1, -1));

        jLabel5.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("E-Mail:");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 130, -1, 30));
        getContentPane().add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 150, 250, 10));

        mailTextField.setBackground(new java.awt.Color(39, 7, 82));
        mailTextField.setFont(new java.awt.Font("Calibri", 1, 11)); // NOI18N
        mailTextField.setForeground(new java.awt.Color(255, 255, 255));
        mailTextField.setBorder(null);
        getContentPane().add(mailTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 130, 250, 20));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/background_recoverypassword.png"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 340, 240));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void changeButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_changeButtonMouseEntered
        changeButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/sendButtonefecto.png")));
    }//GEN-LAST:event_changeButtonMouseEntered

    private void changeButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_changeButtonMouseExited
        changeButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/sendButton.png")));
    }//GEN-LAST:event_changeButtonMouseExited

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(RecoveryPassword.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(RecoveryPassword.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(RecoveryPassword.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RecoveryPassword.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new RecoveryPassword().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton changeButton;
    public javax.swing.JLabel errorLabel;
    private javax.swing.JButton exitButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTextField mailTextField;
    // End of variables declaration//GEN-END:variables
}
