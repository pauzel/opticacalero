package paneles;

import ConnectionSql.ConnectionSQLServer;
import Controller.UserRegistrationAdminEmployeeController;
import Model.UserRegistrationAdminEmployeeModel;
import Misc.Departamentos;
import Misc.HeaderColorRow;
import Misc.HeaderColorTitle;
import java.awt.Cursor;
import static java.awt.Frame.HAND_CURSOR;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;
import javax.swing.Icon;
import javax.swing.JTable;
import Misc.DesignTable;
import Misc.ShowTables;
import java.awt.Toolkit;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

public class registrationPanel extends javax.swing.JPanel {

    ShowTables st = new ShowTables();
    private UserRegistrationAdminEmployeeController URAEC;
    ConnectionSQLServer cc = new ConnectionSQLServer();
    Connection con = cc.getConnection();

    public registrationPanel() {
        initComponents();
        this.PasswordPasswordField.setEchoChar((char) 0);
        setController();
        Departamentos.desparts(DepComboBox);
        Departamentos.municipios(DepComboBox, MunComboBox);
        performance();
    }

    private void performance() {
        st.LoadUserTable(UserTable);
        idTextField.setBackground(new java.awt.Color(0, 0, 0, 1));
        cedFormatTextField.setBackground(new java.awt.Color(0, 0, 0, 1));
        UserTextField.setBackground(new java.awt.Color(0, 0, 0, 1));
        PasswordPasswordField.setBackground(new java.awt.Color(0, 0, 0, 1));
        nameTextField.setBackground(new java.awt.Color(0, 0, 0, 1));
        lastnameTextField.setBackground(new java.awt.Color(0, 0, 0, 1));
        PhoneFormattedTextField.setBackground(new java.awt.Color(0, 0, 0, 1));
        emailTextField.setBackground(new java.awt.Color(0, 0, 0, 1));
        DepComboBox.setBackground(new java.awt.Color(0, 0, 0, 1));
        MunComboBox.setBackground(new java.awt.Color(0, 0, 0, 1));
        RoleComboBox.setBackground(new java.awt.Color(0, 0, 0, 1));
        DirectionTextField.setBackground(new java.awt.Color(0, 0, 0, 1));
        searchuserTextField.setBackground(new java.awt.Color(0, 0, 0, 1));
        this.CleanButton.setCursor(new Cursor(HAND_CURSOR));
        this.SaveButton.setCursor(new Cursor(HAND_CURSOR));
        this.ModifyButton.setCursor(new Cursor(HAND_CURSOR));
        this.DeleteButton.setCursor(new Cursor(HAND_CURSOR));
        this.searchuserButton.setCursor(new Cursor(HAND_CURSOR));
        this.ChangePasswordLabel.setCursor(new Cursor(HAND_CURSOR));
    }

    private void setController() {
        URAEC = new UserRegistrationAdminEmployeeController(this);
        SaveButton.addActionListener(URAEC);
        ModifyButton.addActionListener(URAEC);
        DeleteButton.addActionListener(URAEC);
        CleanButton.addActionListener(URAEC);
    }

    public void setUserData(UserRegistrationAdminEmployeeModel URAEM) {
        idTextField.setText(URAEM.getID_User());
        cedFormatTextField.setText(URAEM.getID_Ced());
        UserTextField.setText(URAEM.getUser());
        PasswordPasswordField.setText(URAEM.getPassword());
        nameTextField.setText(URAEM.getName());
        lastnameTextField.setText(URAEM.getLastName());
        PhoneFormattedTextField.setText(URAEM.getPhone());
        emailTextField.setText(URAEM.getMail());
        DepComboBox.setSelectedItem(URAEM.getDepartment());
        MunComboBox.setSelectedItem(URAEM.getMunicipality());
        RoleComboBox.setSelectedItem(URAEM.getRole());
        DirectionTextField.setText(URAEM.getDirection());
    }

    public UserRegistrationAdminEmployeeModel getUserData() {
        UserRegistrationAdminEmployeeModel URAEM = new UserRegistrationAdminEmployeeModel();
        URAEM.setID_User(idTextField.getText());
        URAEM.setID_Ced(cedFormatTextField.getText());
        URAEM.setUser(UserTextField.getText());
        URAEM.setPassword(String.valueOf(PasswordPasswordField.getPassword()));
        URAEM.setName(nameTextField.getText());
        URAEM.setLastName(lastnameTextField.getText());
        URAEM.setPhone(PhoneFormattedTextField.getText());
        URAEM.setMail(emailTextField.getText());
        URAEM.setDepartment(DepComboBox.getSelectedItem().toString());
        URAEM.setMunicipality(MunComboBox.getSelectedItem().toString());
        URAEM.setRole(RoleComboBox.getSelectedItem().toString());
        URAEM.setDirection(DirectionTextField.getText());
        return URAEM;

    }

    public void Clean() {
        idTextField.setText("");
        cedFormatTextField.setText("");
        UserTextField.setText("");
        PasswordPasswordField.setText("");
        nameTextField.setText("");
        lastnameTextField.setText("");
        PhoneFormattedTextField.setText("");
        emailTextField.setText("");
        DepComboBox.setSelectedItem("");
        MunComboBox.setSelectedItem("");
        RoleComboBox.setSelectedItem("");
        DirectionTextField.setText("");
        errorLabel.setVisible(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSeparator14 = new javax.swing.JSeparator();
        jSeparator11 = new javax.swing.JSeparator();
        jSeparator10 = new javax.swing.JSeparator();
        jSeparator9 = new javax.swing.JSeparator();
        jSeparator8 = new javax.swing.JSeparator();
        jSeparator6 = new javax.swing.JSeparator();
        jSeparator5 = new javax.swing.JSeparator();
        jSeparator4 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator1 = new javax.swing.JSeparator();
        ModifyButton = new javax.swing.JButton();
        DeleteButton = new javax.swing.JButton();
        searchuserButton = new javax.swing.JButton();
        MunComboBox = new javax.swing.JComboBox<>();
        DepComboBox = new javax.swing.JComboBox<>();
        SaveButton = new javax.swing.JButton();
        RoleComboBox = new javax.swing.JComboBox<>();
        DirectionTextField = new javax.swing.JTextField();
        emailTextField = new javax.swing.JTextField();
        lastnameTextField = new javax.swing.JTextField();
        nameTextField = new javax.swing.JTextField();
        idTextField = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        UserTextField = new javax.swing.JTextField();
        PhoneFormattedTextField = new javax.swing.JFormattedTextField();
        errorLabel = new javax.swing.JLabel();
        PasswordPasswordField = new javax.swing.JPasswordField();
        CleanButton = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        UserTable = new javax.swing.JTable();
        searchuserTextField = new app.bolivia.swing.JCTextField();
        ChangePasswordLabel = new javax.swing.JLabel();
        cedFormatTextField = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        background = new javax.swing.JLabel();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        add(jSeparator14, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 320, 270, 10));
        add(jSeparator11, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 100, 240, 10));
        add(jSeparator10, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 70, 160, 10));
        add(jSeparator9, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 200, 440, 10));
        add(jSeparator8, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 160, 160, 10));
        add(jSeparator6, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 100, 160, 10));
        add(jSeparator5, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 70, 160, 10));
        add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 160, 160, 10));
        add(jSeparator3, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 130, 160, 10));
        add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 100, 160, 10));
        add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 70, 60, 10));

        ModifyButton.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        ModifyButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/modifyButton.png"))); // NOI18N
        ModifyButton.setActionCommand("ModifyButton");
        ModifyButton.setBorder(null);
        ModifyButton.setBorderPainted(false);
        ModifyButton.setContentAreaFilled(false);
        ModifyButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                ModifyButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                ModifyButtonMouseExited(evt);
            }
        });
        add(ModifyButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 210, 110, 30));

        DeleteButton.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        DeleteButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/deleteButton.png"))); // NOI18N
        DeleteButton.setActionCommand("DeleteButton");
        DeleteButton.setBorder(null);
        DeleteButton.setBorderPainted(false);
        DeleteButton.setContentAreaFilled(false);
        DeleteButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                DeleteButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                DeleteButtonMouseExited(evt);
            }
        });
        add(DeleteButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 210, -1, 30));

        searchuserButton.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        searchuserButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/searchuserButton.png"))); // NOI18N
        searchuserButton.setBorder(null);
        searchuserButton.setBorderPainted(false);
        searchuserButton.setContentAreaFilled(false);
        searchuserButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                searchuserButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                searchuserButtonMouseExited(evt);
            }
        });
        add(searchuserButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 290, 110, 30));

        MunComboBox.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        MunComboBox.setForeground(new java.awt.Color(255, 255, 255));
        MunComboBox.setBorder(null);
        MunComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MunComboBoxActionPerformed(evt);
            }
        });
        add(MunComboBox, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 140, 160, 20));

        DepComboBox.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        DepComboBox.setForeground(new java.awt.Color(255, 255, 255));
        DepComboBox.setBorder(null);
        DepComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DepComboBoxActionPerformed(evt);
            }
        });
        add(DepComboBox, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 110, 160, 20));

        SaveButton.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N
        SaveButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/insertButton.png"))); // NOI18N
        SaveButton.setActionCommand("InsertButton");
        SaveButton.setBorder(null);
        SaveButton.setBorderPainted(false);
        SaveButton.setContentAreaFilled(false);
        SaveButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                SaveButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                SaveButtonMouseExited(evt);
            }
        });
        add(SaveButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 210, 110, 30));

        RoleComboBox.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        RoleComboBox.setForeground(new java.awt.Color(255, 255, 255));
        RoleComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Admin", "Employee" }));
        RoleComboBox.setBorder(null);
        add(RoleComboBox, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 110, 160, -1));

        DirectionTextField.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        DirectionTextField.setForeground(new java.awt.Color(255, 255, 255));
        DirectionTextField.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        DirectionTextField.setBorder(null);
        add(DirectionTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 140, 160, 20));

        emailTextField.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        emailTextField.setForeground(new java.awt.Color(255, 255, 255));
        emailTextField.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        emailTextField.setBorder(null);
        add(emailTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 80, 240, 20));

        lastnameTextField.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        lastnameTextField.setForeground(new java.awt.Color(255, 255, 255));
        lastnameTextField.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        lastnameTextField.setBorder(null);
        lastnameTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                lastnameTextFieldKeyTyped(evt);
            }
        });
        add(lastnameTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 80, 160, 20));

        nameTextField.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        nameTextField.setForeground(new java.awt.Color(255, 255, 255));
        nameTextField.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        nameTextField.setBorder(null);
        nameTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                nameTextFieldKeyTyped(evt);
            }
        });
        add(nameTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 50, 160, 20));

        idTextField.setEditable(false);
        idTextField.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        idTextField.setForeground(new java.awt.Color(255, 255, 255));
        idTextField.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        idTextField.setBorder(null);
        add(idTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 50, 60, 20));

        jLabel11.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Id_ced");
        add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 80, -1, 30));

        jLabel10.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("Direction:");
        add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 140, -1, 30));

        jLabel9.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("Municipality:");
        add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 140, -1, 30));

        jLabel8.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Departament:");
        add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 110, -1, 30));

        jLabel7.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Email:");
        add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 80, -1, 30));

        jLabel6.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Phone:");
        add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 50, -1, 30));

        jLabel5.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Last name:");
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 80, -1, 30));

        jLabel4.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Id_user:");
        add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 50, -1, 30));

        jLabel3.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Name:");
        add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 50, -1, 30));

        jLabel2.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Role");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 110, -1, 30));

        jLabel1.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("User:");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 110, -1, 30));

        jLabel12.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("Password:");
        add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 140, -1, 30));

        UserTextField.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        UserTextField.setForeground(new java.awt.Color(255, 255, 255));
        UserTextField.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        UserTextField.setBorder(null);
        add(UserTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 110, 160, 20));

        PhoneFormattedTextField.setBorder(null);
        PhoneFormattedTextField.setForeground(new java.awt.Color(255, 255, 255));
        try {
            PhoneFormattedTextField.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("505########")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        PhoneFormattedTextField.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        PhoneFormattedTextField.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        PhoneFormattedTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                PhoneFormattedTextFieldKeyTyped(evt);
            }
        });
        add(PhoneFormattedTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 50, 160, 20));

        errorLabel.setBackground(new java.awt.Color(204, 0, 0));
        errorLabel.setFont(new java.awt.Font("Comic Sans MS", 1, 18)); // NOI18N
        errorLabel.setForeground(new java.awt.Color(255, 0, 0));
        errorLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        add(errorLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 10, 380, 20));

        PasswordPasswordField.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        PasswordPasswordField.setForeground(new java.awt.Color(255, 255, 255));
        PasswordPasswordField.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        PasswordPasswordField.setBorder(null);
        PasswordPasswordField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                PasswordPasswordFieldKeyTyped(evt);
            }
        });
        add(PasswordPasswordField, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 140, 160, 20));

        CleanButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/cleanButton.png"))); // NOI18N
        CleanButton.setActionCommand("CleanButton");
        CleanButton.setBorder(null);
        CleanButton.setBorderPainted(false);
        CleanButton.setContentAreaFilled(false);
        CleanButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                CleanButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                CleanButtonMouseExited(evt);
            }
        });
        add(CleanButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 210, -1, 30));

        UserTable.setForeground(new java.awt.Color(0, 0, 0));
        UserTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Id_user", "Ced", "User", "Password", "Name", "Last_name", "Phone", "Mail", "Departament", "Municipality", "Role", "Direction"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        UserTable.setFocusable(false);
        UserTable.setGridColor(new java.awt.Color(153, 153, 153));
        UserTable.setIntercellSpacing(new java.awt.Dimension(0, 0));
        UserTable.setRowHeight(20);
        UserTable.setSelectionBackground(new java.awt.Color(0, 0, 102));
        UserTable.setSelectionForeground(new java.awt.Color(255, 255, 255));
        UserTable.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        UserTable.getTableHeader().setReorderingAllowed(false);
        UserTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                UserTableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(UserTable);

        add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 330, 900, 170));

        searchuserTextField.setBorder(null);
        searchuserTextField.setForeground(new java.awt.Color(255, 255, 255));
        searchuserTextField.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        searchuserTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        searchuserTextField.setOpaque(false);
        searchuserTextField.setPhColor(new java.awt.Color(255, 255, 255));
        searchuserTextField.setPlaceholder("                       Search User");
        searchuserTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchuserTextFieldActionPerformed(evt);
            }
        });
        searchuserTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                searchuserTextFieldKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                searchuserTextFieldKeyTyped(evt);
            }
        });
        add(searchuserTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 290, 270, 30));

        ChangePasswordLabel.setBackground(new java.awt.Color(0, 204, 0));
        ChangePasswordLabel.setForeground(new java.awt.Color(0, 204, 0));
        ChangePasswordLabel.setText("¿Change Password?, Click");
        ChangePasswordLabel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ChangePasswordLabelMouseClicked(evt);
            }
        });
        add(ChangePasswordLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 170, -1, -1));

        cedFormatTextField.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        cedFormatTextField.setForeground(new java.awt.Color(255, 255, 255));
        cedFormatTextField.setBorder(null);
        cedFormatTextField.setOpaque(false);
        cedFormatTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                cedFormatTextFieldKeyTyped(evt);
            }
        });
        add(cedFormatTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 80, 160, 20));

        jLabel15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/viñetaUser.png"))); // NOI18N
        add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        background.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/background_userRegistration.png"))); // NOI18N
        add(background, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 960, 610));
    }// </editor-fold>//GEN-END:initComponents

    private void MunComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MunComboBoxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_MunComboBoxActionPerformed

    private void DepComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DepComboBoxActionPerformed
        Departamentos.municipios(DepComboBox, MunComboBox);
    }//GEN-LAST:event_DepComboBoxActionPerformed

    private void UserTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_UserTableMouseClicked
        int Selection = UserTable.rowAtPoint(evt.getPoint());
        idTextField.setText(String.valueOf(UserTable.getValueAt(Selection, 0)));
        cedFormatTextField.setText(String.valueOf(UserTable.getValueAt(Selection, 1)));
        UserTextField.setText(String.valueOf(UserTable.getValueAt(Selection, 2)));
        nameTextField.setText(String.valueOf(UserTable.getValueAt(Selection, 4)));
        lastnameTextField.setText(String.valueOf(UserTable.getValueAt(Selection, 5)));
        PhoneFormattedTextField.setText(String.valueOf(UserTable.getValueAt(Selection, 6)));
        emailTextField.setText(String.valueOf(UserTable.getValueAt(Selection, 7)));
        DepComboBox.setSelectedItem(UserTable.getValueAt(Selection, 8));
        MunComboBox.setSelectedItem(UserTable.getValueAt(Selection, 9));
        RoleComboBox.setSelectedItem(UserTable.getValueAt(Selection, 10));
        DirectionTextField.setText(String.valueOf(UserTable.getValueAt(Selection, 11)));
    }//GEN-LAST:event_UserTableMouseClicked

    private void nameTextFieldKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_nameTextFieldKeyTyped
        char validar = evt.getKeyChar();

        if (Character.isDigit(validar)) {
            getToolkit().beep();
            evt.consume();

            errorLabel.setVisible(true);
            errorLabel.setText("Enter only letters in Name field");

        }
    }//GEN-LAST:event_nameTextFieldKeyTyped

    private void lastnameTextFieldKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_lastnameTextFieldKeyTyped
        char validar = evt.getKeyChar();

        if (Character.isDigit(validar)) {
            getToolkit().beep();
            evt.consume();

            errorLabel.setVisible(true);
            errorLabel.setText("Enter only letters in Last name field");

        }
    }//GEN-LAST:event_lastnameTextFieldKeyTyped

    private void PhoneFormattedTextFieldKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_PhoneFormattedTextFieldKeyTyped
        char validar = evt.getKeyChar();

        if (Character.isLetter(validar)) {
            getToolkit().beep();
            evt.consume();

            errorLabel.setVisible(true);
            errorLabel.setText("Enter only numbers in Phone field");
        }
    }//GEN-LAST:event_PhoneFormattedTextFieldKeyTyped

    private void PasswordPasswordFieldKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_PasswordPasswordFieldKeyTyped

        int limite = 100;

        PasswordPasswordField.addKeyListener(new KeyListener() {

            public void keyTyped(KeyEvent e) {
                if (PasswordPasswordField.getText().length() == limite) {
                    e.consume();
                }
            }

            public void keyPressed(KeyEvent arg0) {
            }

            public void keyReleased(KeyEvent arg0) {
            }
        });
    }//GEN-LAST:event_PasswordPasswordFieldKeyTyped

    private void SaveButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_SaveButtonMouseEntered
        SaveButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/insertButtonefecto.png")));
    }//GEN-LAST:event_SaveButtonMouseEntered

    private void SaveButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_SaveButtonMouseExited
        SaveButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/insertButton.png")));
    }//GEN-LAST:event_SaveButtonMouseExited

    private void ModifyButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ModifyButtonMouseEntered
        ModifyButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/modifyButtonefecto.png")));
    }//GEN-LAST:event_ModifyButtonMouseEntered

    private void ModifyButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ModifyButtonMouseExited
        ModifyButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/modifyButton.png")));
    }//GEN-LAST:event_ModifyButtonMouseExited

    private void DeleteButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_DeleteButtonMouseEntered
        DeleteButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/deleteButtonefecto.png")));
    }//GEN-LAST:event_DeleteButtonMouseEntered

    private void DeleteButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_DeleteButtonMouseExited
        DeleteButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/deleteButton.png")));
    }//GEN-LAST:event_DeleteButtonMouseExited

    private void CleanButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_CleanButtonMouseEntered
        CleanButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/CleanButtonefecto.png")));
    }//GEN-LAST:event_CleanButtonMouseEntered

    private void CleanButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_CleanButtonMouseExited
        CleanButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/CleanButton.png")));
    }//GEN-LAST:event_CleanButtonMouseExited

    private void searchuserButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_searchuserButtonMouseEntered
        searchuserButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/searchuserButtonefecto.png")));
    }//GEN-LAST:event_searchuserButtonMouseEntered

    private void searchuserButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_searchuserButtonMouseExited
        searchuserButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/searchuserButton.png")));
    }//GEN-LAST:event_searchuserButtonMouseExited

    private void searchuserTextFieldKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_searchuserTextFieldKeyTyped
        int i = evt.getKeyChar();
        if ((i < '0' || i > '9')) {
            Toolkit.getDefaultToolkit().beep();
            evt.consume();
        }
    }//GEN-LAST:event_searchuserTextFieldKeyTyped

    private void searchuserTextFieldKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_searchuserTextFieldKeyReleased
        String Search = searchuserTextField.getText();
        st.SearchUserTable(UserTable, Search);
    }//GEN-LAST:event_searchuserTextFieldKeyReleased

    private void searchuserTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchuserTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_searchuserTextFieldActionPerformed

    private void ChangePasswordLabelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ChangePasswordLabelMouseClicked
        if (UserTextField.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Por favor seleccione un usuario");
        } else {
            ChangePassword cp = new ChangePassword();
            cp.setVisible(true);
        }

    }//GEN-LAST:event_ChangePasswordLabelMouseClicked

    private void cedFormatTextFieldKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cedFormatTextFieldKeyTyped
        char c = evt.getKeyChar();
        if (!(Character.isDigit(c) || Character.isLetter(c) || c == KeyEvent.VK_BACK_SPACE || c == KeyEvent.VK_ENTER)) {
            evt.consume();
        }
    }//GEN-LAST:event_cedFormatTextFieldKeyTyped


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel ChangePasswordLabel;
    private javax.swing.JButton CleanButton;
    private javax.swing.JButton DeleteButton;
    private javax.swing.JComboBox<String> DepComboBox;
    private javax.swing.JTextField DirectionTextField;
    private javax.swing.JButton ModifyButton;
    private javax.swing.JComboBox<String> MunComboBox;
    private javax.swing.JPasswordField PasswordPasswordField;
    private javax.swing.JFormattedTextField PhoneFormattedTextField;
    private javax.swing.JComboBox<String> RoleComboBox;
    private javax.swing.JButton SaveButton;
    public javax.swing.JTable UserTable;
    private javax.swing.JTextField UserTextField;
    private javax.swing.JLabel background;
    private javax.swing.JTextField cedFormatTextField;
    private javax.swing.JTextField emailTextField;
    public javax.swing.JLabel errorLabel;
    private javax.swing.JTextField idTextField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    public javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator10;
    private javax.swing.JSeparator jSeparator11;
    private javax.swing.JSeparator jSeparator14;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JSeparator jSeparator9;
    private javax.swing.JTextField lastnameTextField;
    private javax.swing.JTextField nameTextField;
    private javax.swing.JButton searchuserButton;
    public static app.bolivia.swing.JCTextField searchuserTextField;
    // End of variables declaration//GEN-END:variables

}
