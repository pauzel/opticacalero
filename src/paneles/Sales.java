package paneles;

import ConnectionSql.ConnectionSQLServer;
import Controller.SalesController;
import Misc.DesignTable;
import Misc.ShowTables;
import Model.SalesModel;
import java.awt.Cursor;
import static java.awt.Frame.HAND_CURSOR;
import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;
import javax.swing.Icon;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

public class Sales extends javax.swing.JPanel {

    ShowTables STB = new ShowTables();
    ConnectionSQLServer cc = new ConnectionSQLServer();
    Connection con = cc.getConnection();

    public Sales() {
        initComponents();
        performance();
    }

    public void performance() {
        setController();
        DesignTable dt = new DesignTable();
        dt.DesignTable(salesTable);
        STB.LoadSalesTable(salesTable);
        this.searchcodeButton.setCursor(new Cursor(HAND_CURSOR));
        this.searchcodeButton.setCursor(new Cursor(HAND_CURSOR));
        this.generatesalesButton.setCursor(new Cursor(HAND_CURSOR));
    }

    public void setController() {
        SalesController sc = new SalesController(this);
        searchidButton.addActionListener(sc);
        searchcodeButton.addActionListener(sc);
        generatesalesButton.addActionListener(sc);
    }

    public SalesModel getSalesData() {
        SalesModel sm = new SalesModel();

        sm.setID_Client(idclientTextField.getText());
        sm.setDate_Exam(examdateTextField.getText());
        sm.setMedication(medicationTextField.getText());
        sm.setObservation(observationTextArea.getText());
        sm.setCode_Prod(codeTextField.getText());
        sm.setBrand(brandTextField.getText());
        sm.setColor(colorTextField.getText());

        return sm;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSeparator7 = new javax.swing.JSeparator();
        jSeparator6 = new javax.swing.JSeparator();
        jSeparator5 = new javax.swing.JSeparator();
        jSeparator4 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator1 = new javax.swing.JSeparator();
        searchinventoryTextField = new app.bolivia.swing.JCTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        salesTable = new javax.swing.JTable();
        generatesalesButton = new javax.swing.JButton();
        previewLabel = new javax.swing.JLabel();
        colorTextField = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        brandTextField = new javax.swing.JTextField();
        searchcodeButton = new javax.swing.JButton();
        codeTextField = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        observationTextArea = new javax.swing.JTextArea();
        jLabel8 = new javax.swing.JLabel();
        medicationTextField = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        searchidButton = new javax.swing.JButton();
        idclientTextField = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        errorLabel = new javax.swing.JLabel();
        examdateTextField = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        add(jSeparator7, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 390, 270, 10));
        add(jSeparator6, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 250, 120, 10));
        add(jSeparator5, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 200, 120, 10));
        add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 150, 60, 10));
        add(jSeparator3, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 290, 140, 10));
        add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 230, 140, 10));
        add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 150, 60, 10));

        searchinventoryTextField.setBackground(new java.awt.Color(255, 255, 255));
        searchinventoryTextField.setBorder(null);
        searchinventoryTextField.setForeground(new java.awt.Color(0, 0, 0));
        searchinventoryTextField.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        searchinventoryTextField.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        searchinventoryTextField.setOpaque(false);
        searchinventoryTextField.setPhColor(new java.awt.Color(102, 102, 102));
        searchinventoryTextField.setPlaceholder("                                 Search Sales");
        searchinventoryTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchinventoryTextFieldActionPerformed(evt);
            }
        });
        searchinventoryTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                searchinventoryTextFieldKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                searchinventoryTextFieldKeyTyped(evt);
            }
        });
        add(searchinventoryTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 360, 270, 30));

        salesTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "ID Client", "Date Exam", "Medication", "Observation", "Code", "Brand", "Color"
            }
        ));
        jScrollPane2.setViewportView(salesTable);

        add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 400, 880, 170));

        generatesalesButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/generate_salesButton.png"))); // NOI18N
        generatesalesButton.setToolTipText("");
        generatesalesButton.setActionCommand("GenerateSales");
        generatesalesButton.setBorderPainted(false);
        generatesalesButton.setContentAreaFilled(false);
        generatesalesButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                generatesalesButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                generatesalesButtonMouseExited(evt);
            }
        });
        add(generatesalesButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 260, 110, 30));

        previewLabel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        add(previewLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(780, 120, 130, 170));

        colorTextField.setEditable(false);
        colorTextField.setBackground(new java.awt.Color(7, 14, 88));
        colorTextField.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        colorTextField.setForeground(new java.awt.Color(255, 255, 255));
        colorTextField.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        colorTextField.setBorder(null);
        add(colorTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 220, 120, 30));

        jLabel13.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("Color:");
        add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 220, -1, 30));

        brandTextField.setEditable(false);
        brandTextField.setBackground(new java.awt.Color(7, 14, 88));
        brandTextField.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        brandTextField.setForeground(new java.awt.Color(255, 255, 255));
        brandTextField.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        brandTextField.setBorder(null);
        add(brandTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 170, 120, 30));

        searchcodeButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/search_codeButton.png"))); // NOI18N
        searchcodeButton.setActionCommand("SearchCode");
        searchcodeButton.setBorderPainted(false);
        searchcodeButton.setContentAreaFilled(false);
        searchcodeButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                searchcodeButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                searchcodeButtonMouseExited(evt);
            }
        });
        add(searchcodeButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 120, 110, 30));

        codeTextField.setBackground(new java.awt.Color(7, 14, 88));
        codeTextField.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        codeTextField.setForeground(new java.awt.Color(255, 255, 255));
        codeTextField.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        codeTextField.setBorder(null);
        add(codeTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 120, 60, 30));

        jLabel12.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("Brand:");
        add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 160, -1, 50));

        jLabel11.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Code:");
        add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 120, -1, 30));

        observationTextArea.setEditable(false);
        observationTextArea.setBackground(new java.awt.Color(7, 14, 88));
        observationTextArea.setColumns(20);
        observationTextArea.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        observationTextArea.setForeground(new java.awt.Color(255, 255, 255));
        observationTextArea.setRows(5);
        jScrollPane1.setViewportView(observationTextArea);

        add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 170, 220, 120));

        jLabel8.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Observation:");
        add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 150, -1, 20));

        medicationTextField.setEditable(false);
        medicationTextField.setBackground(new java.awt.Color(7, 14, 88));
        medicationTextField.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        medicationTextField.setForeground(new java.awt.Color(255, 255, 255));
        medicationTextField.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        medicationTextField.setBorder(null);
        add(medicationTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 260, 140, 30));

        jLabel7.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Date exam:");
        add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 200, -1, 40));

        jLabel6.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Medication:");
        add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 260, -1, 40));

        searchidButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/search_IDButton.png"))); // NOI18N
        searchidButton.setActionCommand("SearchID");
        searchidButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                searchidButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                searchidButtonMouseExited(evt);
            }
        });
        add(searchidButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 120, 110, 30));

        idclientTextField.setBackground(new java.awt.Color(7, 14, 88));
        idclientTextField.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        idclientTextField.setForeground(new java.awt.Color(255, 255, 255));
        idclientTextField.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        idclientTextField.setBorder(null);
        add(idclientTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 120, 60, 30));

        jLabel2.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("ID Client:");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 120, -1, 40));

        errorLabel.setForeground(new java.awt.Color(204, 0, 0));
        errorLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        add(errorLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 70, 280, 20));

        examdateTextField.setBackground(new java.awt.Color(7, 14, 88));
        examdateTextField.setForeground(new java.awt.Color(255, 255, 255));
        examdateTextField.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        examdateTextField.setBorder(null);
        add(examdateTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 204, 140, 30));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/background_sales1.png"))); // NOI18N
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 960, 610));
    }// </editor-fold>//GEN-END:initComponents

    private void searchinventoryTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchinventoryTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_searchinventoryTextFieldActionPerformed

    private void searchinventoryTextFieldKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_searchinventoryTextFieldKeyReleased
         String search = searchinventoryTextField.getText();
         STB.SalesSearchTable(salesTable, search);
    }//GEN-LAST:event_searchinventoryTextFieldKeyReleased

    private void searchinventoryTextFieldKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_searchinventoryTextFieldKeyTyped
        int i = evt.getKeyChar();
        if ((i < '0' || i > '9')) {
            Toolkit.getDefaultToolkit().beep();
            evt.consume();
        }
    }//GEN-LAST:event_searchinventoryTextFieldKeyTyped

    private void searchidButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_searchidButtonMouseEntered
        searchidButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/search_IDButtonefecto.png")));
    }//GEN-LAST:event_searchidButtonMouseEntered

    private void searchcodeButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_searchcodeButtonMouseEntered
        searchcodeButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/search_codeButtonefecto.png")));
    }//GEN-LAST:event_searchcodeButtonMouseEntered

    private void searchidButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_searchidButtonMouseExited
        searchidButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/search_IDButton.png")));
    }//GEN-LAST:event_searchidButtonMouseExited

    private void searchcodeButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_searchcodeButtonMouseExited
        searchcodeButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/search_codeButton.png")));
    }//GEN-LAST:event_searchcodeButtonMouseExited

    private void generatesalesButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_generatesalesButtonMouseEntered
        generatesalesButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/generate_salesButtonefecto.png")));
    }//GEN-LAST:event_generatesalesButtonMouseEntered

    private void generatesalesButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_generatesalesButtonMouseExited
        generatesalesButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/generate_salesButton.png")));
    }//GEN-LAST:event_generatesalesButtonMouseExited


    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JTextField brandTextField;
    private javax.swing.JTextField codeTextField;
    public javax.swing.JTextField colorTextField;
    public javax.swing.JLabel errorLabel;
    public javax.swing.JTextField examdateTextField;
    private javax.swing.JButton generatesalesButton;
    private javax.swing.JTextField idclientTextField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JSeparator jSeparator7;
    public javax.swing.JTextField medicationTextField;
    public javax.swing.JTextArea observationTextArea;
    public javax.swing.JLabel previewLabel;
    public javax.swing.JTable salesTable;
    private javax.swing.JButton searchcodeButton;
    private javax.swing.JButton searchidButton;
    public static app.bolivia.swing.JCTextField searchinventoryTextField;
    // End of variables declaration//GEN-END:variables
}
