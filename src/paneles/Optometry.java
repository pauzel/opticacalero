package paneles;

import ConnectionSql.ConnectionSQLServer;
import Controller.OptometryController;
import Misc.DesignTable;
import Misc.ShowTables;
import Model.OptometryModel;
import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;
import javax.swing.Icon;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

public class Optometry extends javax.swing.JPanel {

    ConnectionSQLServer cc = new ConnectionSQLServer();
    Connection con = cc.getConnection();
    ShowTables STB = new ShowTables();

    public Optometry() {
        initComponents();
        performance();
    }

    public void performance() {
        setController();
        STB.LoadOptometryTable(OptometryTable);
    }

    public void setController() {
        OptometryController oc = new OptometryController(this);
        addpatientsButton.addActionListener(oc);
    }

    public OptometryModel getOptometryData() {
        OptometryModel om = new OptometryModel();
        String EyeR, EyeL, Medication;
        String ar = null, br = null, phc = null;
        EyeR = erefTextField.getText() + ", " + erciTextField.getText() + ", " + erejeTextField.getText() + ", " + eraddTextField.getText();
        EyeL = elefTextField.getText() + ", " + elciTextField.getText() + ", " + elejeTextField.getText() + ", " + eladdTextField.getText();

        if (arCheckBox.isSelected()) {
            ar = "AR";
        } else {
            ar = "";
        }
        if (brCheckBox.isSelected()) {
            br = "BR";
        } else {
            br = "";
        }
        if (phcCheckBox.isSelected()) {
            phc = "PHC";
        } else {
            phc = "";
        }

        Medication = ar + "  " + br + "  " + phc;

        om.setID_Cliente(ID_ClientTextField.getText());
        om.setEyes_Right(EyeR);
        om.setEyes_Left(EyeL);
        om.setDistance(distanceComboBox.getSelectedItem().toString());
        om.setLenses_Type(lensestypeComboBox.getSelectedItem().toString());
        om.setMedication(Medication);
        om.setObservation(observacionTextArea.getText());
        return om;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSeparator11 = new javax.swing.JSeparator();
        jSeparator7 = new javax.swing.JSeparator();
        jSeparator8 = new javax.swing.JSeparator();
        jSeparator9 = new javax.swing.JSeparator();
        jSeparator10 = new javax.swing.JSeparator();
        jSeparator6 = new javax.swing.JSeparator();
        jSeparator5 = new javax.swing.JSeparator();
        jSeparator4 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();
        jSeparator1 = new javax.swing.JSeparator();
        jScrollPane2 = new javax.swing.JScrollPane();
        OptometryTable = new javax.swing.JTable();
        jScrollPane1 = new javax.swing.JScrollPane();
        observacionTextArea = new javax.swing.JTextArea();
        jLabel16 = new javax.swing.JLabel();
        addpatientsButton = new javax.swing.JButton();
        phcCheckBox = new javax.swing.JCheckBox();
        brCheckBox = new javax.swing.JCheckBox();
        arCheckBox = new javax.swing.JCheckBox();
        ID_ClientTextField = new javax.swing.JTextField();
        lensestypeComboBox = new javax.swing.JComboBox<>();
        distanceComboBox = new javax.swing.JComboBox<>();
        elefTextField = new javax.swing.JTextField();
        elciTextField = new javax.swing.JTextField();
        elejeTextField = new javax.swing.JTextField();
        eladdTextField = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        eraddTextField = new javax.swing.JTextField();
        erejeTextField = new javax.swing.JTextField();
        erciTextField = new javax.swing.JTextField();
        erefTextField = new javax.swing.JTextField();
        searchinventoryTextField = new app.bolivia.swing.JCTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        errorLabel = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        add(jSeparator11, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 380, 270, 10));
        add(jSeparator7, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 270, 30, 10));
        add(jSeparator8, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 270, 30, 10));
        add(jSeparator9, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 270, 30, 10));
        add(jSeparator10, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 270, 30, 10));
        add(jSeparator6, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 230, 30, 10));
        add(jSeparator5, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 230, 30, 10));
        add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 230, 30, 10));
        add(jSeparator3, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 230, 30, 10));
        add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 160, 50, 10));

        OptometryTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null}
            },
            new String [] {
                "ID client", "Exam Date", "Eyes Right", "Eyes Left", "Distance", "Lenses Type", "Medication", "Observation"
            }
        ));
        jScrollPane2.setViewportView(OptometryTable);

        add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 400, 900, 170));

        observacionTextArea.setBackground(new java.awt.Color(7, 14, 88));
        observacionTextArea.setColumns(20);
        observacionTextArea.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        observacionTextArea.setForeground(new java.awt.Color(255, 255, 255));
        observacionTextArea.setRows(5);
        jScrollPane1.setViewportView(observacionTextArea);

        add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(640, 140, 270, 160));

        jLabel16.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(255, 255, 255));
        jLabel16.setText("Observation:");
        add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 120, -1, -1));

        addpatientsButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/add_patients.png"))); // NOI18N
        addpatientsButton.setActionCommand("addpatients");
        addpatientsButton.setBorderPainted(false);
        addpatientsButton.setContentAreaFilled(false);
        addpatientsButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                addpatientsButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                addpatientsButtonMouseExited(evt);
            }
        });
        add(addpatientsButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 260, 110, 30));

        phcCheckBox.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        phcCheckBox.setForeground(new java.awt.Color(255, 255, 255));
        phcCheckBox.setText("PHC");
        phcCheckBox.setContentAreaFilled(false);
        add(phcCheckBox, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 220, -1, -1));

        brCheckBox.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        brCheckBox.setForeground(new java.awt.Color(255, 255, 255));
        brCheckBox.setText("BR");
        brCheckBox.setContentAreaFilled(false);
        add(brCheckBox, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 220, -1, -1));

        arCheckBox.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        arCheckBox.setForeground(new java.awt.Color(255, 255, 255));
        arCheckBox.setText("AR");
        arCheckBox.setContentAreaFilled(false);
        add(arCheckBox, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 220, -1, -1));

        ID_ClientTextField.setBackground(new java.awt.Color(7, 14, 88));
        ID_ClientTextField.setForeground(new java.awt.Color(255, 255, 255));
        ID_ClientTextField.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        ID_ClientTextField.setBorder(null);
        add(ID_ClientTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 140, 50, 20));

        lensestypeComboBox.setBackground(new java.awt.Color(7, 14, 88));
        lensestypeComboBox.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        lensestypeComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Monofocal", "Bifocal", "Progressive" }));
        lensestypeComboBox.setBorder(null);
        add(lensestypeComboBox, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 180, 120, -1));

        distanceComboBox.setBackground(new java.awt.Color(7, 14, 88));
        distanceComboBox.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        distanceComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Only far", "Only near", "Both" }));
        distanceComboBox.setBorder(null);
        add(distanceComboBox, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 140, 120, -1));

        elefTextField.setBackground(new java.awt.Color(7, 14, 88));
        elefTextField.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        elefTextField.setForeground(new java.awt.Color(255, 255, 255));
        elefTextField.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        elefTextField.setBorder(null);
        add(elefTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 250, 30, 20));

        elciTextField.setBackground(new java.awt.Color(7, 14, 88));
        elciTextField.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        elciTextField.setForeground(new java.awt.Color(255, 255, 255));
        elciTextField.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        elciTextField.setBorder(null);
        add(elciTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 250, 30, 20));

        elejeTextField.setBackground(new java.awt.Color(7, 14, 88));
        elejeTextField.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        elejeTextField.setForeground(new java.awt.Color(255, 255, 255));
        elejeTextField.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        elejeTextField.setBorder(null);
        add(elejeTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 250, 30, 20));

        eladdTextField.setBackground(new java.awt.Color(7, 14, 88));
        eladdTextField.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        eladdTextField.setForeground(new java.awt.Color(255, 255, 255));
        eladdTextField.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        eladdTextField.setBorder(null);
        add(eladdTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 250, 30, 20));

        jLabel12.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("ADD");
        add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 190, -1, -1));

        jLabel11.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("EJE");
        add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 190, -1, -1));

        jLabel10.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("CI");
        add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 190, -1, -1));

        jLabel9.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("EF");
        add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 190, -1, -1));

        eraddTextField.setBackground(new java.awt.Color(7, 14, 88));
        eraddTextField.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        eraddTextField.setForeground(new java.awt.Color(255, 255, 255));
        eraddTextField.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        eraddTextField.setBorder(null);
        add(eraddTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 210, 30, 20));

        erejeTextField.setBackground(new java.awt.Color(7, 14, 88));
        erejeTextField.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        erejeTextField.setForeground(new java.awt.Color(255, 255, 255));
        erejeTextField.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        erejeTextField.setBorder(null);
        add(erejeTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 210, 30, 20));

        erciTextField.setBackground(new java.awt.Color(7, 14, 88));
        erciTextField.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        erciTextField.setForeground(new java.awt.Color(255, 255, 255));
        erciTextField.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        erciTextField.setBorder(null);
        add(erciTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 210, 30, 20));

        erefTextField.setBackground(new java.awt.Color(7, 14, 88));
        erefTextField.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        erefTextField.setForeground(new java.awt.Color(255, 255, 255));
        erefTextField.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        erefTextField.setBorder(null);
        add(erefTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 210, 30, 20));

        searchinventoryTextField.setBackground(new java.awt.Color(255, 255, 255));
        searchinventoryTextField.setBorder(null);
        searchinventoryTextField.setForeground(new java.awt.Color(0, 0, 0));
        searchinventoryTextField.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        searchinventoryTextField.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        searchinventoryTextField.setOpaque(false);
        searchinventoryTextField.setPhColor(new java.awt.Color(153, 153, 153));
        searchinventoryTextField.setPlaceholder("                       Search Inventory");
        searchinventoryTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchinventoryTextFieldActionPerformed(evt);
            }
        });
        searchinventoryTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                searchinventoryTextFieldKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                searchinventoryTextFieldKeyTyped(evt);
            }
        });
        add(searchinventoryTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 350, 270, 30));

        jLabel8.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Lenses Type:");
        add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 180, -1, 20));

        jLabel7.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Distance: ");
        add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 140, -1, 20));

        jLabel6.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Eyes Left");
        add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 250, -1, 30));

        jLabel5.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Eyes Right");
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 210, -1, 30));

        jLabel4.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Medication:");
        add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 220, -1, 20));

        jLabel2.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("ID Client:");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 140, -1, 30));

        errorLabel.setForeground(new java.awt.Color(204, 0, 0));
        errorLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        errorLabel.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        add(errorLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 90, 340, -1));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/background_optometry1.png"))); // NOI18N
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 960, 610));
    }// </editor-fold>//GEN-END:initComponents

    private void addpatientsButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addpatientsButtonMouseEntered
        addpatientsButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/add_patientsefecto.png")));
    }//GEN-LAST:event_addpatientsButtonMouseEntered

    private void addpatientsButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addpatientsButtonMouseExited
        addpatientsButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/resources/Buttons/add_patients.png")));
    }//GEN-LAST:event_addpatientsButtonMouseExited

    private void searchinventoryTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchinventoryTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_searchinventoryTextFieldActionPerformed

    private void searchinventoryTextFieldKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_searchinventoryTextFieldKeyReleased
        String Search = searchinventoryTextField.getText();
        STB.OptometrySearchTable(OptometryTable, Search);
    }//GEN-LAST:event_searchinventoryTextFieldKeyReleased

    private void searchinventoryTextFieldKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_searchinventoryTextFieldKeyTyped
        int i = evt.getKeyChar();
        if ((i < '0' || i > '9')) {
            Toolkit.getDefaultToolkit().beep();
            evt.consume();
        }
    }//GEN-LAST:event_searchinventoryTextFieldKeyTyped


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField ID_ClientTextField;
    public javax.swing.JTable OptometryTable;
    private javax.swing.JButton addpatientsButton;
    private javax.swing.JCheckBox arCheckBox;
    private javax.swing.JCheckBox brCheckBox;
    private javax.swing.JComboBox<String> distanceComboBox;
    private javax.swing.JTextField eladdTextField;
    private javax.swing.JTextField elciTextField;
    private javax.swing.JTextField elefTextField;
    private javax.swing.JTextField elejeTextField;
    private javax.swing.JTextField eraddTextField;
    private javax.swing.JTextField erciTextField;
    private javax.swing.JTextField erefTextField;
    private javax.swing.JTextField erejeTextField;
    public javax.swing.JLabel errorLabel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator10;
    private javax.swing.JSeparator jSeparator11;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JSeparator jSeparator9;
    private javax.swing.JComboBox<String> lensestypeComboBox;
    private javax.swing.JTextArea observacionTextArea;
    private javax.swing.JCheckBox phcCheckBox;
    public static app.bolivia.swing.JCTextField searchinventoryTextField;
    // End of variables declaration//GEN-END:variables
}
